package com.cp4space.catagolue.servlets;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PostSynthesisServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        boolean is_admin = (userService.isUserLoggedIn() && userService.isUserAdmin());

        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();
        
        String content = req.getParameter("content");

        if (content == null) {
            writer.println("No RLE was provided.");
            return;
        }

        StringBuilder sb = new StringBuilder("x = 0, y = 0, rule = B3/S23\n");
        String[] lines = content.split("\n");

        boolean consumedHeader = false;
        boolean bang = false;
        int totalpop = 0;

        for (String rawline : lines) {
            String line = rawline.trim();
            if (line.length() == 0) { continue; } // blank line
            if (line.charAt(0) == '#') { continue; } // comment

            if (consumedHeader) {

                line = line.replace("B", "b"); // erase history

                int digitrun = 0;
                int runlength = 0;
                for (int i = 0; i < line.length(); i++) {
                    char c = line.charAt(i);

                    if ((c >= '0') && (c <= '9')) {
                        digitrun += 1;
                        runlength = runlength * 10 + (c - '0');
                        if (digitrun >= 5) {
                            writer.println("Invalid: line '" + line + "' contains 5 consecutive digits.");
                            return;
                        }
                    } else {
                        digitrun = 0;
                        if (c == '!') {
                            bang = true;
                        } else if ((c == 'o') || (c == 'A')) {
                            if (runlength == 0) { runlength = 1; }
                            totalpop += runlength;
                        } else if ((c != '$') && (c != 'b') && (c != '.')) {
                            writer.println("Invalid: line '" + line + "' contains illegal character " + c);
                            return;
                        }
                        runlength = 0;
                    }
                }
                if (digitrun > 0) {
                    writer.println("Invalid: line '" + line + "' ends in a digit.");
                    return;
                }
                sb.append(line + "\n");
                if (bang) { break; }
            } else {
                if (line.charAt(0) != 'x') {
                    writer.println("Invalid: line '" + line + "' must begin with an 'x'.");
                    return;
                }
                consumedHeader = true;
            }
        }

        if (!bang) {
            writer.println("Invalid: RLE must end with an exclamation mark.");
            return;
        }

        String rle = sb.toString();
        int totalbytes = rle.length();

        writer.println("RLE is well-formed and has a population of " +
                    totalpop + " and a size of " + totalbytes + " bytes.");

        if (totalpop > 100000) {
            writer.println("Invalid: RLE must not exceed 100000 cells.");
            return;
        }

        if (totalbytes > 410000) {
            writer.println("Invalid: RLE must not exceed 400 kilobytes.");
            return;
        }

        writer.println("RLE is sufficiently small to post to Catagolue:\n");
        writer.println(rle);

        Date date = new Date();
        Entity rleEntity = new Entity("SynthRLE");
        rleEntity.setProperty("date", date);
        rleEntity.setProperty("content", new Text(rle));
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        datastore.put(rleEntity);

        writer.println("RLE has been successfully saved in the Datastore.");
    }

}
