package com.cp4space.catagolue.utils;

public class NumberUtils {
    
    /**
    * Format a long integer in a human-friendly manner. This will only
    * retain between 2 and 4 significant figures (inclusive) of the
    * integer, and uses the US (short-scale) conventions:
    *
    *    thousand == 10^3;
    *     million == 10^6;
    *     billion == 10^9;
    *    trillion == 10^12;
    * quadrillion == 10^15;
    */
    public static String illion(long x) {
        if (x < 0) {
            return ("-" + illion(-x));
        } else if (x == 0) {
            return "zero";
        } else {
            // We don't need to go to 'quintillion' as 2^63 < 10^19:
            String[] expnames = {"", " thousand", " million", " billion", " trillion", " quadrillion"};
            long y = x;
            int exponent = 0;
            while (y >= 10000) {
                y = y / 1000;
                exponent += 1;
            }
            return String.valueOf(y) + expnames[exponent];
        }
    }

    public static String commaChameleon(long value) {
        if (value < 0) {
            return "&#8722;" + commaChameleon(-value);
        } else if (value == 999999999999999999L) {
            return "infinity";
        } else if (value >= 100000000000000000L) {
            return commaChameleon(value % 100000000000000000L);
        } else if (value >= 1000) {
            return commaChameleon(value / 1000) + "&#8201;" + String.valueOf((value % 1000) + 1000).substring(1);
        } else {
            return String.valueOf(value);
        }
    }

}
