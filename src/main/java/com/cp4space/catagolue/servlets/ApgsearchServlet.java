package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.ConcurrentModificationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.GzipUtil;
import com.cp4space.catagolue.servlets.ObjectServlet;
import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

public class ApgsearchServlet extends ExampleServlet {

    static int UNASSIGNED = 0;
    static int CENSUS_COUNTS = 1;
    static int TOP_SOUPS = 2;
    static int SAMPLE_SOUPIDS = 3;

    public static Date bumpCensus(DatastoreService datastore, String rulestring, String symmetries) {

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetries);
        Date date = new Date();

        try {
            // Update (or, if necessary, create) the parent census:
            // Do this really quickly to make it practically atomic:
            try {
                Entity census = datastore.get(censusKey);
                long uncommittedCount = (Math.max(0, (long) census.getProperty("uncommittedHauls"))) + 1;
                census.setProperty("uncommittedHauls", uncommittedCount);
                census.setProperty("lastModified", date);
                if (uncommittedCount == 1) {
                    datastore.put(census);
                }

            } catch (EntityNotFoundException e) {

                Entity census = new Entity("Census", rulestring + "/" + symmetries);
                census.setProperty("uncommittedHauls", 1);
                census.setProperty("committedHauls", 0);
                census.setProperty("symmetries", symmetries);
                census.setProperty("rulestring", rulestring);
                census.setProperty("numsoups", 0);
                census.setProperty("numobjects", 0);
                census.setProperty("lastModified", date);
                datastore.put(census);
            }
        } catch (ConcurrentModificationException e) {
            // do nothing
        }

        return date;

    }

    void postApgsearchHaul(PrintWriter writer, BufferedReader reader, String displayedName) {

        String line = null;

        String seedroot = null;
        String symmetries = "C1";
        String rulestring = "b3s23";
        String version = "unknown";

        StringBuilder censusData = new StringBuilder();
        StringBuilder soupData = new StringBuilder();
        StringBuilder topData = new StringBuilder();

        long numsoups = -1;
        long numobjects = -1;

        int readingMode = UNASSIGNED;

        try {
            while ((line = reader.readLine()) != null) {

                /*
                * These special characters should not occur in the uploaded
                * file and can be used for HTML injection. Consequently, we
                * strip these from the string to ensure properly-formatted
                * HTML on the client side.
                */
                line = line.replace("&", "").replace("<", "").replace(">", "").replace("'", "").replace("\"", "");

                if (line.length() >= 2) {

                    if (line.charAt(0) == '@') {
                        // Header line:
                        String[] parts = line.substring(1).split(" ", 2);

                        try {
                            if (parts[0].equalsIgnoreCase("ROOT") && parts.length >= 2) {
                                seedroot = parts[1];
                            } else if (parts[0].equalsIgnoreCase("RULE") && parts.length >= 2) {
                                rulestring = parts[1].toLowerCase();
                            } else if (parts[0].equalsIgnoreCase("VERSION") && parts.length >= 2) {
                                version = parts[1];
                            } else if (parts[0].equalsIgnoreCase("SYMMETRY") && parts.length >= 2) {
                                symmetries = parts[1];
                            } else if (parts[0].equalsIgnoreCase("NUM_SOUPS")) {
                                numsoups = Long.valueOf(parts[1]);
                            } else if (parts[0].equalsIgnoreCase("NUM_OBJECTS")) {
                                numobjects = Long.valueOf(parts[1]);
                            } else if (parts[0].equalsIgnoreCase("CENSUS")) {
                                readingMode = CENSUS_COUNTS;
                            } else if (parts[0].equalsIgnoreCase("SAMPLE_SOUPIDS")) {
                                readingMode = SAMPLE_SOUPIDS;
                            } else if (parts[0].equalsIgnoreCase("TOP")) {
                                readingMode = TOP_SOUPS;
                            }
                        } catch (NumberFormatException e) {
                            // To stop saboteurs from crashing Catagolue by posting invalid data.
                            writer.println("An exception was encountered due to incorrectly-formatted results:");
                            e.printStackTrace(writer);
                            return;
                        }
                    } else {
                        // Ordinary line:
                        if (readingMode == CENSUS_COUNTS) {
                            censusData.append(line + "\n");
                        } else if (readingMode == SAMPLE_SOUPIDS) {
                            soupData.append(line + "\n");
                        } else if (readingMode == TOP_SOUPS) {
                            topData.append(line + "\n");
                        }
                        if (censusData.length() + soupData.length() + topData.length() >= 1500000) {
                            writer.println("Haul size exceeds limit of 1500 KB uncompressed.");
                            return;
                        }
                    }
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            writer.println("An exception was encountered due to incorrectly-formatted results:");
            e.printStackTrace(writer);
            return;
        } catch (IOException e) {
            writer.println("An input/output exception was encountered:");
            e.printStackTrace(writer);
            return;
        }

        long lakh = 100000;
        long quadrillion = lakh * lakh * lakh;

        long minsoups = 5000;

        if (symmetries.contains("stdin")) {
            minsoups = 1;
        }

        if (rulestring.equals("b3s23") && ObjectServlet.isOfficialSquare(symmetries)) {
            minsoups = 500000;
        }

        long minobjs = minsoups * 20;

        if (!displayedName.contains("goucher")) {
            if (seedroot == null) {
                writer.println("A seed root must be provided.");
                return;
            } else if ((seedroot.length() < 12) || (seedroot.length() > 44)) {
                writer.println("Seed root must be between 12 and 44 characters in length.");
                return;
            } else if (!rulestring.matches("[a-z][a-zA-Z0-9_-]+")) {
                writer.println("Malformed rulestring.");
                return;
            } else if (!symmetries.matches("([A-Z][a-zA-Z0-9_]*~)?[a-zA-Z0-9_+]+")) {
                writer.println("Malformed symmetries.");
                return;
            } else if ((numsoups < minsoups) && (numobjects < minobjs)) {
                writer.println("Insufficiently many soups/objects reported.");
                return;
            } else if ((numsoups >= quadrillion) || (numobjects >= quadrillion)) {
                writer.println("Inordinately many soups/objects reported.");
                return;
            } else if ((numsoups < 0) || (numobjects < 0)) {
                writer.println("Negative number of soups/objects reported.");
                return;
            } else if (numobjects == 0) {
                writer.println("Haul contains no objects: perhaps the rule/symmetry combination always results in no objects being produced?");
                return;
            } else if (version.contains("makeshift")) {
                writer.println("Get a B3/S23 and stop spamming Catagolue.");
                return;
            }
        }

        // Add the haul to the Datastore:
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(seedroot.getBytes("UTF-8"));

            String haulcode = PayoshaUtils.bytesToHex(md.digest());

            Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetries).addChild("Haul", haulcode).getKey();

            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

            if (haulExists(datastore, haulKey)) {
                writer.println("Haul already exists.");
                return;

            } else {

                Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetries);
                Date date = bumpCensus(datastore, rulestring, symmetries);

                // The haul should belong to its parent census:
                Entity haul = new Entity("Haul", haulcode, censusKey);
                haul.setProperty("displayedName", displayedName);
                haul.setProperty("date", date);
                haul.setUnindexedProperty("root", seedroot);
                haul.setUnindexedProperty("version", version);

                haul.setUnindexedProperty("compressed", true);
                byte[] comp1 = GzipUtil.zip(soupData.toString(), "gz");
                haul.setProperty("soupData", new Blob(comp1));
                byte[] comp2 = GzipUtil.zip(censusData.toString(), "gz");
                haul.setProperty("censusData", new Blob(comp2));

                haul.setProperty("topData", new Text(topData.toString()));

                int comcode = Census.JUST_UPLOADED;
                if (version.equals("v3.23")) { comcode = 4; }
                if (version.contains("v5.3") && (censusData.toString().contains("ov_"))) { comcode = 4; }

                haul.setProperty("committed", comcode);
                haul.setUnindexedProperty("numsoups", numsoups);
                haul.setUnindexedProperty("numobjects", numobjects);

                Census.singleput(datastore, haul);
            }

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public boolean haulExists(DatastoreService datastore, Key haulKey) {

        try {
            datastore.get(haulKey);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }

    }

    @Override
    public String getTitle(HttpServletRequest req) {
        return "apgsearch";
    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {
        writer.println("<p>The Ash Pattern Generator (or apgsearch) is a cross-platform program for "
                + " running random initial configurations in Conway's Game of Life and examining "
                + " all of the objects produced. The current recommended version is 5.x:</p>");

        writer.println("<h3>Feature comparison</h3>");

        writer.println("<table><tr><th></th><th>Version 5.x (C++)</th><th>Version 1.x (Python)</th></tr>");
        writer.println("<tr><th>Typical speed (per core)*</th><td>7200 soups/sec</td><td>300 soups/sec</td></tr>");
        writer.println("<tr><th>Multi-core searching</th><td>Yes</td><td>No</td></tr>");
        writer.println("<tr><th>Dependencies</th><td>GCC, Python</td><td>Python, Golly</td></tr>");
        writer.println("<tr><th>Requires compilation</th><td>Yes</td><td>No</td></tr></table>");

        writer.println("<p>*on an Intel(R) Core(TM) i5-4590 CPU @ 3.30GHz.</p>");

        writer.println("<p>A more comprehensive comparison is available on");
        writer.println("<a href=\"https://conwaylife.com/wiki/Apgsearch#Versions\">the wiki page</a>");
        writer.println("and <a href=\"https://conwaylife.com/wiki/Tutorials/Contributing_to_Catagolue\">Catagolue tutorial</a>.</p>");

        writer.println("<h3>Version 5.x (C++)</h3>");

        writer.println("<p>The source code and instructions are on");
        writer.println("<a href=\"https://gitlab.com/apgoucher/apgmera\">the GitLab repository</a>.</p>");

        writer.println("<p>Alternatively, there is a precompiled Windows version of");
        writer.println("<a href=\"/binaries/apgluxe-windows-x86_64.exe\">apgsearch v4.xxxx</a>.</p>");

        writer.println("<p>Version 5.x (built from source) can also run on CUDA-capable GPUs.</p>");

        writer.println("<h3>Version 1.x (legacy)</h3>");

        writer.println("<p>You need compatible versions of Python 2 and Golly together with the search script:</p>");

        writer.println("<ul>");
        writer.println("<li><a href=\"/binaries/apgsearch-2015-05-25.py\">apgsearch v1.1</a></li>");
        writer.println("<li><a href=\"https://www.python.org/download/releases/2.7/\">Python 2.7</a>");
        writer.println("(already installed on Mac OS X and most Linux distributions)</li>");
        writer.println("<li><a href=\"http://sourceforge.net/projects/golly/files/\">Golly</a> (strictly earlier than 4.0)</li>");
        writer.println("</ul>");

        writer.println("<p>You'll also need to generate a <a href=\"/payosha256\">payosha256 key</a>");
        writer.println("unless you want to upload your soups anonymously (in which case use the key '#anon').</p>");

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();

        String hostname = req.getRemoteHost();
        if (hostname == null) { hostname = "null"; }

        String payoshaRequest = reader.readLine();

        String payoshaResponse = PayoshaUtils.callPayosha("catagolue", payoshaRequest);
        String[] parts = payoshaResponse.split(":", 4);

        if ((parts[0].equals("payosha256")) && (parts[1].equals("good")) && (parts[2].equals("post_apgsearch_haul"))) {
            writer.println("Payosha256 authentication succeeded.");
            postApgsearchHaul(writer, reader, parts[3]);
        } else {
            writer.println("Payosha256 authentication failed with the following response:");
            writer.println(payoshaResponse);
        }

        writer.println("***********************************************");
    }
}

