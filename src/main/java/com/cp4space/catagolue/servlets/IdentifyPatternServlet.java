package com.cp4space.catagolue.servlets;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

import com.cp4space.catagolue.algorithms.SmallLife;

public class IdentifyPatternServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        boolean is_admin = (userService.isUserLoggedIn() && userService.isUserAdmin());

        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();
        
        String content = req.getParameter("content");

        if (content == null) {
            writer.println("No RLE was provided.");
            return;
        }

        String[] lines = content.split("\n");

        boolean consumedHeader = false;
        boolean bang = false;

        int x = 0;
        int y = 0;

        int[][] plane = new int[400][400];

        String rulestring = "b3s23";

        for (String rawline : lines) {
            String line = rawline.trim();
            if (line.length() == 0) { continue; } // blank line
            if (line.charAt(0) == '#') { continue; } // comment

            if (consumedHeader) {

                int runlength = 0;
                for (int i = 0; i < line.length(); i++) {
                    char c = line.charAt(i);

                    if ((c >= '0') && (c <= '9')) {
                        runlength = runlength * 10 + (c - '0');
                        if (runlength >= 400) {
                            writer.println("Invalid: RLE exceeds 400x400 box.");
                            return;
                        }
                    } else {
                        if (runlength == 0) { runlength = 1; }
                        if (c == '!') {
                            bang = true; runlength = 0; break;
                        } else if (c == '$') {
                            x = 0; y += runlength;
                            if (y >= 400) {
                                writer.println("Invalid: RLE exceeds 400x400 box.");
                                return;
                            }
                        } else if ((c == 'o') || (c == 'A')) {
                            for (int j = 0; j < runlength; j++) {
                                if (x >= 400) {
                                    writer.println("Invalid: RLE exceeds 400x400 box.");
                                    return;
                                }
                                plane[y][x] = 1;
                                x += 1;
                            }
                        } else {
                            x += runlength;
                        }
                        runlength = 0;
                    }
                }
                if (runlength > 0) {
                    writer.println("Invalid: line '" + line + "' ends in a digit.");
                    return;
                }
                if (bang) { break; }
            } else {
                if (line.charAt(0) != 'x') {
                    writer.println("Invalid: line '" + line + "' must begin with an 'x'.");
                    return;
                }
                consumedHeader = true;
                if (line.contains("rule")) {
                    rulestring = line.split("rule", 2)[1].toLowerCase().trim();
                    if ((rulestring.length() == 0) || (rulestring.charAt(0) != '=')) {
                        writer.println("Invalid: rule keyword must be followed by an '='.");
                        return;
                    }
                    rulestring = rulestring.substring(1).replace("/", "").trim();
                }
            }
        }

        if (!bang) {
            writer.println("Invalid: RLE must end with an exclamation mark.");
            return;
        }

        String regex = "b[1-8ceaiknjqrytwz-]*s[0-8ceaiknjqrytwz-]*";

        if (rulestring.equals("life")) { rulestring = "b3s23"; }
        if (rulestring.equals("highlife")) { rulestring = "b36s23"; }
        if (rulestring.equals("drylife")) { rulestring = "b37s23"; }
        if (rulestring.equals("pedestrianlife")) { rulestring = "b38s23"; }

        if (!(Pattern.matches(regex, rulestring))) {
            writer.println("Invalid: rulestring " + rulestring + " does not match regex " + regex);
            return;
        }

        int maxgens = 10000;
        int compbudget = 10000000;
        String apgcode = SmallLife.compute_apgcode(plane, rulestring, maxgens, compbudget);

        if (apgcode.equals("unknown")) {
            writer.println("Pattern does not become periodic within computational budget.");
            return;
        }

        resp.sendRedirect("https://catagolue.hatsya.com/object/" + apgcode + "/" + rulestring);

    }

}
