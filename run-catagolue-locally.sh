#!/bin/bash

bash configure-viewer.sh

set -e

mvn clean install
exec 3< <( mvn -B appengine:run 2>&1 )

while read line; do

    echo "$line"
    if echo "$line" | grep 'is running at'; then
        server_host="$( echo "$line" | grep -o "http://[^ /]*" )"
        break
    fi

done <&3

sleep 1

if [ -z "$server_host" ]; then
    printf "\n\033[31;1mError:\033[0m could not run Catagolue locally.\n\n"
    exit 1
fi

printf "\nInitialising Catagolue at\033[32;1m $server_host/home \033[0m...\n\n"

cd initialise
python3 process.py "test_password" "$server_host"

while read line; do
    echo "$line"
done <&3
