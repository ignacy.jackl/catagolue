package com.cp4space.catagolue.parmenides;

import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.Map.Entry;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.patterns.GolObject;
import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;

public class Parmenides {

    public static final int commonThreshold = 300;

    public static String remd5(String md5) {

        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update((md5 + "_subbookkeeper").getBytes("UTF-8"));
            return PayoshaUtils.bytesToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            return "INVALID";
        } catch (UnsupportedEncodingException e) {
            return "INVALID";
        }

    }

    public static double typicalChisquare(String rulestring, String symmetry, Entity cachedCensus) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, Census.JUST_COMMITTED);
        Query query2 = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery2 = datastore.prepare(query2);
        int maxhauls = 50;
        List<Entity> hauls = preparedQuery2.asList(FetchOptions.Builder.withLimit(maxhauls));

        double chisquaresum = 0.0;
        double totalhauls = 0.0;

        for (Entity haul : hauls) {
            Census haulCensus = new Census();
            String root = (String) haul.getProperty("root");
            long claimedobjs = (long) haul.getProperty("numobjects");
            haulCensus.affineAdd(Tabulation.getArbitraryData(haul, "censusData"), 1);
            haulCensus.includeSamples(Tabulation.getArbitraryData(haul, "soupData"), symmetry, root, null);
            double chisquaretotal = Parmenides.chisquare(cachedCensus, haulCensus, claimedobjs);
            chisquaresum += chisquaretotal;
            totalhauls += 1.0;
        }

        double tcs = (chisquaresum / totalhauls);
        cachedCensus.setProperty("meanchisquare", tcs);
        return tcs;
    }

    public static Entity retrieveTable(String rulestring, String symmetry) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key statsKey = KeyFactory.createKey("ParmenidesCache", rulestring + "/" + symmetry);

        try {
            Entity cachedCensus = datastore.get(statsKey);
            Double tcs = (Double) cachedCensus.getProperty("meanchisquare");
            if (tcs == null) {
                return regenerateTable(rulestring, symmetry);
            } else {
                return cachedCensus;
            }
        } catch (EntityNotFoundException e) {
            return regenerateTable(rulestring, symmetry);
        }
    }

    public static Entity regenerateTable(String rulestring, String symmetry) {

        int maxobjs = commonThreshold;

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity cachedCensus = new Entity("ParmenidesCache", rulestring + "/" + symmetry);

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        Query query = new Query("Tabulation", censusKey);
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> tabulations = preparedQuery.asList(FetchOptions.Builder.withLimit(1000));

        Census census = new Census();
        long totalobjects = 0;

        for (Entity tabulation : tabulations) {
            Census lcensus = new Census();
            lcensus.affineAdd(Tabulation.getCensusData(tabulation), 1);

            int i = 0;
            for (Map.Entry<String, Long> entry : lcensus.sortedTable()) {
                totalobjects += entry.getValue();
                i += 1;
                if (i <= maxobjs) { census.table.put(entry.getKey(), entry.getValue()); }
            }
        }

        Tabulation tnew = new Tabulation("commonest");

        int i = 0;

        for (Map.Entry<String, Long> entry : census.sortedTable()) {
            String apgcode = entry.getKey();
            long quantity = entry.getValue();

            if (quantity != 0) {
                tnew.appendRow(apgcode, quantity);
            }

            i += 1;
            if (i >= maxobjs) { break; }
        }

        cachedCensus.setProperty("numobjects", totalobjects);
        tnew.writeTab(cachedCensus);
        typicalChisquare(rulestring, symmetry, cachedCensus);
        datastore.put(cachedCensus);

        return cachedCensus;

    }

    public static void rareObjects(String rulestring, Entity cachedCensus, Census haulCensus, StringBuilder privateData, StringBuilder publicData) {

        Census census = new Census();
        census.affineAdd(Tabulation.getCensusData(cachedCensus), 1);
        Map<String, Long> table = census.table;
        TreeSet<Entry<String,Long>> sortedTable = census.sortedTable();
        TreeSet<Entry<String,Long>> haulSortedTable = haulCensus.sortedTable();

        int i = 0;

        long occurrenceThreshold = 0;

        for (Entry<String, Long> entry : sortedTable) {
            long occurrences = entry.getValue();

            i++;

            if (i == commonThreshold) {
                occurrenceThreshold = 2*occurrences;
            }
        }

        for (Entry<String, Long> entry : haulSortedTable) {

            String apgcode = entry.getKey();

            if (table.containsKey(apgcode) && table.get(apgcode) >= occurrenceThreshold) {
                // writer.println("       " + apgcode);
            } else if ((apgcode.length() > 2) && (apgcode.charAt(0) == 'm') && (apgcode.charAt(1) == 'e')) {
                // Not a thing
            } else {
                // writer.println("****** " + apgcode);

                privateData.append(apgcode+" "+String.valueOf(entry.getValue())+" "+((new GolObject(rulestring, apgcode)).validatePattern() ? "true" : "false") + "\n");

                if (haulCensus.samples.containsKey(apgcode)) {
                    for (String sampleString : haulCensus.samples.get(apgcode)) {
                        publicData.append(sampleString + "\n");
                    }
                }
            }

        }

        privateData.append("\n");
        publicData.append("\n");

    }

    public static double chisquare(Entity cachedCensus, Census haulCensus, long claimedobjs) {

        Census census = new Census();
        census.affineAdd(Tabulation.getCensusData(cachedCensus), 1);
        TreeSet<Entry<String,Long>> sortedTable = census.sortedTable();
        TreeSet<Entry<String,Long>> haulSortedTable = haulCensus.sortedTable();

        long totalObjects = (long) cachedCensus.getProperty("numobjects");
        long haulTotalObjects = 0;

        for (Entry<String, Long> entry : haulSortedTable) {
            haulTotalObjects += entry.getValue();
        }

        if (claimedobjs > 0) { haulTotalObjects = claimedobjs; }

        double chisquaretotal = 0;
        double chisquaremax = 0;

        int i = 0;

        for (Entry<String, Long> entry : sortedTable) {
            long occurrences = entry.getValue();

            i++;

            if ((i <= commonThreshold) && (i >= 10)) {
                String apgcode = entry.getKey();

                long haulOccurrences = (haulCensus.table.containsKey(apgcode) ? haulCensus.table.get(apgcode) : 0);

                double observed = haulOccurrences;
                double expected = ((((double) haulTotalObjects) * ((double) occurrences))/((double) totalObjects));
                double chisquare = ((observed - expected) * (observed - expected)) / expected;
                // writer.println(apgcode + ":                 " + String.valueOf(chisquare));

                chisquaretotal += chisquare;
                chisquaremax = Math.max(chisquaremax, chisquare);
            }
        }

        return chisquaretotal;
    }

}
