package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.payosha256.PayoshaUtils;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.census.Census;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

public class CommonNamesServlet extends HttpServlet {

    public static void deleteSynths(int number_to_delete) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query query = new Query("SynthRLE").addSort("date", Query.SortDirection.ASCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query.setKeysOnly());
        Iterable<Entity> synths = preparedQuery.asIterable(FetchOptions.Builder.withLimit(number_to_delete));
        List<Key> itemsToDelete = new ArrayList<Key>();
        for (Entity entity : synths) { itemsToDelete.add(entity.getKey()); }
        datastore.delete(itemsToDelete);

    }

    public static void updateSynths(String rulestring, BufferedReader reader) throws IOException {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Census pseudocensus = new Census();
        String line = null;
        String apgcode = null;
        StringBuilder synthData = null;
        Map<String, String> syntheses = new HashMap<String, String>();

        while ((line = reader.readLine()) != null) {
            if ((apgcode == null) && (line.length() >= 8) && (line.substring(0, 8).equals("#CSYNTH "))) {
                String[] parts = line.split(" ");
                apgcode = parts[1];
                long glidercost = Long.valueOf(parts[3]);
                if (line.contains("(true)")) { glidercost += 100000000000000000L; }
                pseudocensus.table.put(apgcode, glidercost);
                if (glidercost == 999999999999999999L) {
                    apgcode = null;
                } else {
                    synthData = new StringBuilder();
                }
            }

            if (apgcode != null) {
                synthData.append(line + "\n");
                if ((line.length() >= 1) && (line.charAt(0) != '#') && (line.contains("!"))) {
                    syntheses.put(apgcode, synthData.toString());
                    apgcode = null;
                }
            }
        }

        Census.updateMetadatumParallel(datastore, rulestring, "synthesis", syntheses, false);

        String symmetry = "synthesis-costs";

        pseudocensus.updateTabulations(rulestring, symmetry, null, null, true);

        {
            Entity census = new Entity("Census", rulestring + "/" + symmetry);
            census.setProperty("uncommittedHauls", 0);
            census.setProperty("committedHauls", 0);
            census.setProperty("symmetries", symmetry);
            census.setProperty("rulestring", rulestring);
            census.setProperty("numsoups", -1);
            census.setProperty("numobjects", -1);
            census.setProperty("lastModified", new Date());
            datastore.put(census);
        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();
        
        String line = reader.readLine();
        String password = line.split(" ")[0];

        if (!PayoshaUtils.validatePassword(password)) {
            writer.println("Invalid password.");
            return;
        } else {
            writer.println("Valid password.");
        }

        String command = line.split(" ")[1];
        String namespace = line.split(" ")[2];

        Map<String, String> namemap = new HashMap<String, String>();

        if (command.equals("UPDATE")) {
            CommonNames.getNamemap(namespace, namemap, false);
        } else if (command.equals("OVERWRITE")) {
            // do nothing;
        } else if (command.equals("DELSYNTH")) {
            deleteSynths(Integer.valueOf(namespace));
            writer.println("Operation successful.");
            return;
        } else if (command.equals("SYNTH")) {
            updateSynths(namespace, reader);
            writer.println("Operation successful.");
            return;
        } else {
            writer.println("Unrecognised operation");
            return;
        }
        
        while ((line = reader.readLine()) != null) {
            if (line.length() >= 2) {
                String[] parts = line.split(" ", 2);
                namemap.put(parts[0], parts[1]);
            }
        }

        CommonNames.putNamemap(namespace, namemap);

        writer.println("Operation successful.");
    }
}
