package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

import org.pegdown.Parser;
import org.pegdown.PegDownProcessor;
import org.pegdown.LinkRenderer;
import org.pegdown.VerbatimSerializer;

import com.cp4space.catagolue.utils.RleVerbatimSerializer;
import com.cp4space.catagolue.utils.SvgUtils;

public class ExampleServlet extends HttpServlet {

    /**
     * Auto-generated by Eclipse:
     */
    private static final long serialVersionUID = 1L;


    public String getTitle(HttpServletRequest req) {
        return "Test page";
    }

    public void writeNavbar(PrintWriter writer, String firstString, String... strings) {

        writer.println("<div id=\"cl_navbar\">");

        writer.println(firstString);

        for (String link : strings) {
            writer.println("&nbsp;&nbsp;&bull;&nbsp;&nbsp;");
            writer.println(link.replace("href=\"/", "href=\"https://catagolue.hatsya.com/"));
        }

        writer.println("</div>");

    }

    private static final String[] adverts = {"cgolbook.png;https://conwaylife.com/book/"};

    public void writeRandomAdvert(PrintWriter writer) {

        int advertNumber = (int) (Math.random() * 3.0 * adverts.length);
        if (advertNumber < adverts.length) {
            String[] x = adverts[advertNumber].split(";");
            writer.println("<p><br /><br /><a href=\"" + x[1] + "\"><img src=\"/images/footers/" + x[0] + "\" width=\"900\" height=\"320\"/></a></p>");
        }
    }

    public void writeMainNavbar(PrintWriter writer) {

        writeNavbar(writer,
                "<a href=\"/home\">Home</a>",
                "<a href=\"/rules\">Rules</a>",
                "<a href=\"/object\">Objects</a>",
                "<a href=\"/census\">Census</a>",
                "<a href=\"/software\">Software</a>",
                "<a href=\"/syntheses\">Syntheses</a>",
                "<a href=\"/statistics\">Statistics</a>");
                // "<a href=\"/nfts\">NFTs</a>");
        //        "<a href=\"/help/index.htm\">Help</a>");

    }

    public void writeHead(PrintWriter writer, HttpServletRequest req) {

    }

    public static boolean isChristmas() {
        String todaysdate = new SimpleDateFormat("MM-dd").format(new Date());
        boolean xmas = ((todaysdate.compareTo("12-01") >= 0) && (todaysdate.compareTo("12-25") <= 0));
        return xmas;
    }

    public void writeImage(PrintWriter writer, HttpServletRequest req) {

        boolean xmas = isChristmas();

        if (xmas) {
            writer.println("<div id=\"title\"><img src=\"/images/santagolue.png\"></div>");
        } else {
            writer.println("<div id=\"title\"><img src=\"/images/catagolue.png\"></div>");
        }

    }

    public void writeNavbars(PrintWriter writer, HttpServletRequest req) {

        writeMainNavbar(writer);

    }

    public void writeComments(PrintWriter writer, HttpServletRequest req, String threadName, boolean commentable) {

        String specifiedOffset = req.getParameter("offset");

        int commentOffset = ((specifiedOffset == null) ? 0 : Integer.valueOf(specifiedOffset));

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key threadKey = KeyFactory.createKey("CommentThread", threadName);
        Query query = new Query("Comment", threadKey).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);
        int commentCount = preparedQuery.countEntities(FetchOptions.Builder.withLimit(1000));
        List<Entity> comments = preparedQuery.asList(FetchOptions.Builder.withLimit(20).offset(commentOffset));

        writer.println("<h2>Comments (" + (commentCount == 1000 ? "1000+" : commentCount) + ")</h2>");

        if (comments.isEmpty()) {

            if (commentCount == 0) {
                writer.println("<p>There are no comments to display.</p>");
            } else {
                writer.println("<p>You have reached the end of the comment thread.</p>");
            }

        } else {

            writer.println("<p>Displaying comments "+(commentOffset+1)+" to "+(commentOffset+comments.size())+".</p>");

            PegDownProcessor markdownProcessor = new PegDownProcessor(Parser.FENCED_CODE_BLOCKS | Parser.STRIKETHROUGH | Parser.SUPPRESS_INLINE_HTML | Parser.SUPPRESS_HTML_BLOCKS);

            Map<String, VerbatimSerializer> map = new HashMap<String, VerbatimSerializer>() {{
                put("rle", new RleVerbatimSerializer());
            }};

            for (Entity comment : comments) {

                Text commentContentText = (Text) comment.getProperty("content");

                String commentContent = (commentContentText == null) ? null : commentContentText.getValue();

                if (commentContent == null) {
                    commentContent = "_comment not found_";
                }

                writer.println("<div id=\"comment\">");
                writer.println("<div id=\"comtent\">");
                Date date = (Date) comment.getProperty("date");
                User op = (User) comment.getProperty("user");
                writer.println("<p>On " + (new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z").format(date)) + ", ");
                if (op == null) {
                    writer.println("Someone wrote:</p>");
                } else {
                    String opn = op.getNickname().replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("'", "&quot;");
                    Boolean iua = (Boolean) comment.getProperty("admin");
                    if ((iua != null) && (iua)) {
                        opn = "<font color=\"green\">" + opn + "</font>";
                    }
                    writer.println("<b>" + opn + "</b> wrote:</p>");
                }
                writer.println(markdownProcessor.markdownToHtml(commentContent, new LinkRenderer(), map));
                writer.println("</div></div>");

            }
        }

        if (commentable) {
            if (req.getUserPrincipal() == null) {
                writer.println("<p>Please log in to post comments.</p>");
            } else {
                writeCommentBox(writer, req, threadName);
            }
        }

    }

    public void writeComment(PrintWriter writer, String comment) {

        PegDownProcessor markdownProcessor = new PegDownProcessor(Parser.FENCED_CODE_BLOCKS | Parser.STRIKETHROUGH);

        Map<String, VerbatimSerializer> map = new HashMap<String, VerbatimSerializer>() {{
            put("rle", new RleVerbatimSerializer());
        }};

        writer.println("<div id=\"comment\">");
        writer.println("<div id=\"comtent\">");
        writer.println(markdownProcessor.markdownToHtml(comment, new LinkRenderer(), map));
        writer.println("</div></div>");

    }

    public void writeSynthesisBox(PrintWriter writer, HttpServletRequest req) {
        writer.println("<h3>Post new synthesis</h3>");
        writer.println("<form action=\"/postsynth\" method=\"post\">");
        writer.println("<div><textarea name=\"content\" rows=\"12\" cols=\"100\"></textarea></div>");
        writer.println("<div><input type=\"submit\" value=\"Post\"/></div>");
        writer.println("</form>");
    }

    public void writeIdentifyBox(PrintWriter writer, HttpServletRequest req) {
        writer.println("<h3>Identify pattern</h3>");
        writer.println("<p>Copy an RLE (max size 400x400) of an oscillator, spaceship, or still-life " +
                        "into this box to identify it.</p>");
        writer.println("<form action=\"/identify\" method=\"post\">");
        writer.println("<div><textarea name=\"content\" rows=\"12\" cols=\"100\"></textarea></div>");
        writer.println("<div><input type=\"submit\" value=\"Identify\"/></div>");
        writer.println("</form>");
    }

    public void writeCommentBox(PrintWriter writer, HttpServletRequest req, String threadName) {
        writer.println("<h3>Post new comment</h3>");
        writer.println("<form action=\"/postcomment\" method=\"post\">");
        writer.println("<div><textarea name=\"content\" rows=\"12\" cols=\"100\"></textarea></div>");
        writer.println("<div><input type=\"submit\" value=\"Post\"/></div>");
        writer.println("<input type=\"hidden\" name=\"threadName\" value=\"" + threadName + "\"/>");
        writer.println("<input type=\"hidden\" name=\"redirectLocation\" value=\"" + req.getRequestURI() + "\"/>");
        writer.println("</form>");
    }

    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        writer.println("<p><b><span style=\"color:red\">&bull; &lArr;</span><span style=\"color:green\">&rArr; &bull;</span></b></p>");

        writer.println("<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
                "incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation " +
                "ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in " +
                "reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat " +
                "cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>");
        writer.println("<h3>Subheading</h3>");
        writer.println("<p>Here is some more generic filler text.</p>");

        writer.println("<table cellspacing=1 border=2 cols=2>");

        writer.println("<tr><th>Image</th><th>Object</th><th>Common name</th></tr>");

        SvgUtils.apgRow(writer, "b3s23", "xs4_33", "block");
        SvgUtils.apgRow(writer, "b3s23", "xq4_27deee6", "heavyweight spaceship");
        SvgUtils.apgRow(writer, "b3s23", "xp3_co9nas0san9oczgoldlo0oldlogz1047210127401", "pulsar");
        SvgUtils.apgRow(writer, "b3s23", "xp2_2a54", "clock");
        SvgUtils.apgRow(writer, "b3s23", "xp46_330279cx1aad3y833zx4e93x855bc", "twin bees shuttle");
        SvgUtils.apgRow(writer, "b3s23", "xs20_3lkkl3z32w23", "mirrored dock");
        SvgUtils.apgRow(writer, "b3s23", "xq3_3u0228mc53bgzof0882d6koq1", "turtle");

        writer.println("</table>");

        writeSynthesisBox(writer, req);

        writeComments(writer, req, "example_thread", false);

        /*
           writeComment(writer,
           "#### Heading\n" +
           "\n" +
           "Words can be rendered in:\n" +
           "\n" +
           "- **bold**;\n" +
           "- _italics_;\n" +
           "- ~~strikethrough~~.\n" +
           "\n" +
           "This is just a generic paragraph, potentially spanning multiple lines and\n" +
           "using github_flavoured_markdown. This is followed by an image of an eater2,\n" +
           "for no particularly good reason other than to show how to include images.\n" +
           "\n" +
           "![eater2](/images/eater2.png)\n" +
           "\n" +
           "And here is an RLE code block:\n" +
           "\n" +
           "```rle\n" +
           "#C A p15 glider reflector\n" +
           "#C The constellation of block, boat and eater reflects a glider and is\n" +
           "#C repaired by the intervention of a spark from Karel Suhajda's p15,\n" +
           "#C which is itself reliant on a pentadecathlon for support.\n" +
           "x = 21, y = 17, rule = B3/S23\n" +
           "10b2o6b2o$9bo2bo4bo2bo$9bo2bo4bo2bo$o8bo2bo4bo2bo$3o7b2o6b2o$3bo$2b2o$\n" +
           "13bo2bo$5bo5b3o2b3o$4bobo3bo8bo$2o2b2o5b2o4b2o$2o10bo4bo3$2b2o$3b2o$2b\n" +
           "o!\n" +
           "```");
         */

    }

    @Override
        public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException {

            String title = getTitle(req);
            String title2 = title;

            String[] parts2 = title.split("!");
            if (parts2.length == 2) {
                title = parts2[1];
                title2 = parts2[1] + parts2[0];
            } else if (parts2.length >= 3) {
                title = parts2[1];
                title2 = "<a href=\"https://conwaylife.com/wiki/" + parts2[2] + "\">" + parts2[1] + "</a>" + parts2[0];
            }

            // This contains the template for Catagolue pages:
            resp.setContentType("text/html");
            PrintWriter writer = resp.getWriter();
            writer.println("<!DOCTYPE html>");
            writer.println("<html>");

            writer.println("<head>");
            writer.println("<title>");
            writer.println(title + " - Catagolue");
            writer.println("</title>");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"/apple-touch-icon-57x57.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"/apple-touch-icon-60x60.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/apple-touch-icon-72x72.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"/apple-touch-icon-76x76.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/apple-touch-icon-114x114.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"/apple-touch-icon-120x120.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"/apple-touch-icon-144x144.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"/apple-touch-icon-152x152.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"/apple-touch-icon-180x180.png?v=QE740lWWn9\" />");
            writer.println("<link rel=\"icon\" type=\"image/png\" href=\"/favicon-32x32.png?v=QE740lWWn9\" sizes=\"32x32\" />");
            writer.println("<link rel=\"icon\" type=\"image/png\" href=\"/favicon-96x96.png?v=QE740lWWn9\" sizes=\"96x96\" />");
            writer.println("<link rel=\"icon\" type=\"image/png\" href=\"/favicon-16x16.png?v=QE740lWWn9\" sizes=\"16x16\" />");
            writer.println("<link rel=\"manifest\" href=\"/manifest.json?v=QE740lWWn9\" />");
            writer.println("<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"/favicon.ico?v=QE740lWWn9\" />");
            writer.println("<meta name=\"msapplication-TileColor\" content=\"#00aba9\" />");
            writer.println("<meta name=\"msapplication-TileImage\" content=\"/mstile-144x144.png?v=QE740lWWn9\" />");
            writer.println("<meta name=\"theme-color\" content=\"#c0ffee\" />");
            writer.println("<link type=\"text/css\" rel=\"stylesheet\" href=\"/stylesheets/main.css\"/>");
            writeHead(writer, req);
            writer.println("</head>");

            writer.println("<body>");
            writer.println("<div id=\"container\">");
            writer.println("<div id=\"body\">");
            writer.println("<div id=\"centerDiv\">");
            writer.println("<div id=\"cl_navbar\" style=\"text-align:right\">");

            Principal ofInduction = req.getUserPrincipal();
            UserService userService = UserServiceFactory.getUserService();

            String uri = req.getRequestURI();
            if (req.getQueryString() != null) { uri += "?" + req.getQueryString(); }

            try {
                if (ofInduction == null) {
                    // User is not logged in
                    writer.println("<b>(anonymous)</b>" +
                            "&nbsp;&nbsp;&bull;&nbsp;&nbsp;<a href=\"" + userService.createLoginURL(uri) +
                            "\">log in</a>&nbsp;&nbsp;");
                } else {
                    // User is logged in
                    writer.println("<b>" + ofInduction.getName() + "</b>" +
                            "&nbsp;&nbsp;&bull;&nbsp;&nbsp;<a href=\"" + userService.createLogoutURL(uri) +
                            "\">log out</a>&nbsp;&nbsp;");
                }
            } catch (IllegalArgumentException e) {
                // do nothing
            }

            writer.println("</div>");

            writeImage(writer, req);
            writeNavbars(writer, req);

            writer.println("<div id=\"content\">");
            writer.println("<h2 style=\"overflow-wrap:break-word\">" + title2 + "</h2>");
            writeContent(writer, req);
            // writeRandomAdvert(writer);
            writer.println("</div>");

            writer.println("<div id=\"cl_navbar\"><i>");
            writer.println("<b>Catagolue</b> &mdash; the largest distributed search of cellular automata.");
            writer.println("</i></div>");

            writer.println("</div>");
            writer.println("</div>");
            writer.println("</div>");
            writer.println("</body>");

            writer.println("</html>");
        }
}
