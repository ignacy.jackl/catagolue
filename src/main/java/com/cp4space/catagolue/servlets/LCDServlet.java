package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.lang.Iterable;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Map;
import java.util.HashMap;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.FetchOptions;

public class LCDServlet extends HttpServlet {

    public static void generateContent(PrintWriter writer, String rulestring, String symmetry) {

        writer.println("// Proof-of-work difficulty table, normalised such");
        writer.println("// that the pentadecathlon has unit difficulty.");

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        try {

            Entity censusEntity = datastore.get(censusKey);
            long numsoups = (long) censusEntity.getProperty("numsoups");
            if (numsoups < 0) { numsoups = 0 - numsoups; }
            writer.println("// soup count: " + numsoups);

        } catch (EntityNotFoundException e) { }

        Query query = new Query("Tabulation", censusKey);
        PreparedQuery preparedQuery = datastore.prepare(query);
        Iterable<Entity> tabulations = preparedQuery.asIterable(FetchOptions.Builder.withLimit(1000));

        Map<String, Long> tcomb = new HashMap<String, Long>();

        long totobj = 0;
        long corderpuffers = 0;

        for (Entity tabulation : tabulations) {
            String tabstring = Tabulation.getCensusData(tabulation);
            String tabname = tabulation.getKey().getName();
            if (((tabname.charAt(0) == 'x') || (tabname.charAt(0) == 'y') ||
                (tabname.equals("methuselah"))) && (tabname.charAt(1) != 's')) {
                String[] parts = tabstring.split("\n");
                long miscobj = 0;
                for (String part : parts) {
                    String[] parts2 = part.split(" ");
                    long ocount = Long.valueOf(parts2[1]);
                    miscobj += ocount;
                }
                totobj += miscobj;

                int i = 0; int j = 0;
                for (String part : parts) {
                    String[] parts2 = part.split(" ");
                    long ocount = Long.valueOf(parts2[1]);
                    miscobj -= ocount;
                    i += 1;
                    if ((i <= 25) && (ocount >= miscobj + 3)) { j = i; }
                }
                i = 0;
                for (String part : parts) {
                    String[] parts2 = part.split(" ");
                    long ocount = Long.valueOf(parts2[1]);
                    if (i < j) {
                        tcomb.put(parts2[0], ocount);
                    } else {
                        miscobj += ocount;
                    }
                    i += 1;
                }

                if (miscobj > 0) {
                    if (tabname.charAt(0) == 'y') {
                        long period = Long.valueOf(tabname.substring(2));
                        if ((period != 1152) && ((period % 96 == 0) || (period == 144))) {
                            corderpuffers += miscobj;
                        } else {
                            tcomb.put(tabname, miscobj);
                        }
                    } else {
                        tcomb.put(tabname, miscobj);
                    }
                }
            }
        }

        if (corderpuffers > 0) { tcomb.put("corderpuffer", corderpuffers); }

        String x = Census.serialiseSortTable(tcomb);
        String[] parts = x.split("\n");
        long remobj = totobj;
        int maxlen = 0;

        for (String part : parts) {
            String apgcode = part.split(" ")[1];
            int alen = apgcode.length();
            if (alen > maxlen) { maxlen = alen; }
        }

        maxlen += 1;
        String spaces = " ";
        while (spaces.length() <= maxlen) { spaces = spaces + spaces; }
        spaces = spaces.substring(0, maxlen);

        long pentadecathla = 0;

        for (String part : parts) {
            String[] parts2 = part.split(" ");
            String apgcode = parts2[1];

            if (apgcode.equals("xp15_4r4z4r4")) { pentadecathla = remobj; }
            long ratio = pentadecathla / remobj;

            String preamble = "difficulties[" + spaces.substring(apgcode.length()) + "\"" + apgcode + "\"] = ";
            writer.println(preamble + ratio + ".0;");
            remobj -= Long.valueOf(parts2[0]);
        }
    }

    public static String writeContent(PrintWriter writer, String rulestring, String symmetry) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key statsKey = KeyFactory.createKey("Difficulties", rulestring + "/" + symmetry);

        try {
            Entity statsEntity = datastore.get(statsKey);
            String content = ((Text) statsEntity.getProperty("data")).getValue();
            Date lastModified = (Date) statsEntity.getProperty("lastModified");

            Date currentTime = new Date();

            long elapsedMillis = currentTime.getTime() - lastModified.getTime();
            long elapsedSeconds = elapsedMillis / 1000;

            if (elapsedSeconds < 600) {
                String creationTime = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastModified));
                if (writer != null) {
                    writer.println("// The following page is cached from a copy generated at " + creationTime + ".");
                    writer.println("// It is " + elapsedSeconds + " seconds old.");
                    writer.println(content);
                }
                return content;
            }
        } catch (EntityNotFoundException e) {
            
        }

        ByteArrayOutputStream bbuffer = new ByteArrayOutputStream();
        PrintWriter writer2 = new PrintWriter(bbuffer);
        generateContent(writer2, rulestring, symmetry);
        writer2.close();

        String content = bbuffer.toString();

        Entity statsEntity = new Entity("Difficulties", rulestring + "/" + symmetry);
        Date date = new Date();
        statsEntity.setProperty("lastModified", date);
        statsEntity.setProperty("data", new Text(content));

        datastore.put(statsEntity);

        if (writer != null) {
            writer.println("// The following page has been generated from the current census data:");
            writer.println(content);
        }

        return content;
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        /*
        String hostname = req.getRemoteHost();
        if (hostname == null) { hostname = "null"; }
        writer.println("// Host name: " + hostname);
        */

        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
        }

        PrintWriter writer = resp.getWriter();
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        if (rulestring == null) {
            writer.println("You must specify a rulestring.");
        } else if (symmetry == null) {
            writer.println("You must specify a symmetry type.");
        } else {
            writeContent(writer, rulestring, symmetry);
        }
    }
}
