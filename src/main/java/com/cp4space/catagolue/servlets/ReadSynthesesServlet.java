package com.cp4space.catagolue.servlets;

import java.io.PrintWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.patterns.GolObject;
import com.cp4space.payosha256.PayoshaUtils;
import com.cp4space.catagolue.utils.SvgUtils;
import com.cp4space.catagolue.servlets.HashsoupServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;


public class ReadSynthesesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Query query = new Query("SynthRLE").addSort("date", Query.SortDirection.ASCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);
        Iterable<Entity> synths = preparedQuery.asIterable(FetchOptions.Builder.withLimit(300));

        PrintWriter writer = resp.getWriter();

        for (Entity entity : synths) {
            Text synthText = (Text) entity.getProperty("content");
            writer.println(synthText.getValue());
        }

    }
}
