package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.servlets.ObjectServlet;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.Soup;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.apphosting.api.ApiProxy;

public class BackupServlet extends HttpServlet {

    public static String getTodaysDate() {

        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());

    }

    public static String tryBackup(PrintWriter writer, String rulestring, String symmetry) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);
        String todaysdate = getTodaysDate();
        Key censusKey2 = KeyFactory.createKey("Census", rulestring + "/" + symmetry + "-" + todaysdate);

        try {
            Entity censusEntity = datastore.get(censusKey);
            Entity censusCopy = new Entity(censusKey2);
            censusCopy.setPropertiesFrom(censusEntity);
            censusCopy.setProperty("numobjects", 0 - (long) censusEntity.getProperty("numobjects"));
            censusCopy.setProperty("numsoups", 0 - (long) censusEntity.getProperty("numsoups"));
            censusCopy.setProperty("uncommittedHauls", (long) 0);
            censusCopy.setProperty("lastModified", new Date(1));
            datastore.put(censusCopy);

            Query query = new Query("Tabulation", censusKey);
            PreparedQuery preparedQuery = datastore.prepare(query);
            List<Entity> tabulations = preparedQuery.asList(FetchOptions.Builder.withLimit(1000));

            for (Entity tabulation : tabulations) {
                String prefix = tabulation.getKey().getName();
                Entity tab = new Entity("Tabulation", prefix, censusKey2);
                tab.setPropertiesFrom(tabulation);
                datastore.put(tab);
            }

            writer.println("Backup completed.");

            try {

                Key lbkey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Leaderboard", "numobjects").getKey();
                Key lbkey2 = new KeyFactory.Builder("Census", rulestring + "/" + symmetry + "-" + todaysdate).addChild("Leaderboard", "numobjects").getKey();

                Entity lbEntity = datastore.get(lbkey);
                Entity lbCopy = new Entity(lbkey2);
                lbCopy.setPropertiesFrom(lbEntity);
                datastore.put(lbCopy);

            } catch (EntityNotFoundException e) {
                writer.println("Leaderboard does not exist.");
            }

        } catch (EntityNotFoundException e) {
            writer.println("Census does not exist.");
        }

        return todaysdate;

    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();

        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 3) && (symmetry == null)) {
                rulestring = pathParts[1];
                symmetry = pathParts[2];
            }
        }

        if ((rulestring == null) || (symmetry == null)) {
            writer.println("Please provide a rule string and symmetry type.");
        } else if (ObjectServlet.isUnofficial(symmetry, rulestring) != 0) {
            writer.println("You cannot create a backup of an unofficial symmetry.");
        } else if (rulestring.equals("b3s23") && (symmetry.equals("C1") || symmetry.equals("G1")) && (ApiProxy.getCurrentEnvironment().getRemainingMillis() < 120000)) {
            writer.println("You cannot manually backup the main b3s23/C1 census.");
        } else {
            String todaysdate = tryBackup(writer, rulestring, symmetry);
            writer.println("todaysdate = " + todaysdate);
            if (rulestring.equals("b3s23") && symmetry.equals("C1")) {

                // Daily backup of G1:
                tryBackup(writer, rulestring, "G1");

                if (todaysdate.substring(todaysdate.length() - 1).equals("5")) {
                    // Thrice-monthly backup of synthesis costs:
                    tryBackup(writer, rulestring, "synthesis-costs");
                } else if (todaysdate.substring(todaysdate.length() - 2).equals("01")) {
                    // Monthly backup of all higher symmetries:
                    String[] offsyms = {"1x256", "2x128", "4x64", "8x32", // asymmetric;
                        "C2_1", "C2_2", "C2_4", "C4_1", "C4_4", // cyclic;
                        "D2_x", "D2_+1", "D2_+2", "D8_1", "D8_4", // dihedral;
                        "D4_+1", "D4_+2", "D4_+4", "D4_x1", "D4_x4"}; // dihedral;

                    for (String highersym : offsyms) {
                        if (ApiProxy.getCurrentEnvironment().getRemainingMillis() > 120000) {
                            tryBackup(writer, rulestring, highersym);
                        }
                    }
                }

                // Create asymmetric-soups census by combining C1 and G1:
                String[] components = {"C1", "G1"};
                Census.unionCensuses("b3s23", components, "asymmetric-soups");
            }
        }
    }
}
