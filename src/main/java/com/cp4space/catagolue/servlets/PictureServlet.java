package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.utils.SvgUtils;

public class PictureServlet  extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException {
        
        resp.setContentType("image/svg+xml");
        
        String rulestring = req.getParameter("rule");
        String apgcode = req.getParameter("apgcode");
        
        String maxsizeString = req.getParameter("maxsize");
        int maxsize = ((maxsizeString == null) ? 640 : Integer.valueOf(maxsizeString));
        
        String maxcellString = req.getParameter("maxcell");
        int maxcell = ((maxcellString == null) ? 20 : Integer.valueOf(maxcellString));
        
        String pathinfo = req.getPathInfo();
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (apgcode == null)) {
                apgcode = pathParts[1];
            }
            if ((pathParts.length >= 3) && (rulestring == null)) {
                rulestring = pathParts[2];
            }
        }
        
        if (rulestring == null) {
            rulestring = "b3s23";
        }
        
        PrintWriter writer = resp.getWriter();
        
        SvgUtils.apgToSvg(writer, rulestring, apgcode, maxsize, maxsize, maxcell);

    }
}

