#!/bin/bash

HASH="$( sha256sum /tmp/client-secret.json )"
HASH="${HASH:0:64}"
printf "Hash: \033[32;1m $HASH \033[0m\n"

if [ "$HASH" != "fe871d4726d0f849477eac1755833a334c8323be6a6a84e5d7f915f7e174416b" ]; then
echo "Warning: skipping Catagolue update as client secret is incorrect"
exit 0
fi

set -e

# new method:
curl https://sdk.cloud.google.com > install.sh
bash install.sh --disable-prompts 2>/dev/null 1>/dev/null
PATH=~/google-cloud-sdk/bin:$PATH
gcloud auth activate-service-account --key-file=/tmp/client-secret.json
mvn -B appengine:deploy

# old deprecated method:
# mvn -B appengine:update -Dappengine.additionalParams="--service_account_json_key_file=/tmp/client-secret.json"

HASH="$( sha512sum /tmp/client-secret.json )"
HASH="${HASH:0:40}"
# Do NOT print the hash here; it should be secret!
cd initialise
python3 process.py "$HASH" "https://catagolue.hatsya.com"
