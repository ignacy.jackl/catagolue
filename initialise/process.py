#!/usr/bin/python3

import os
import re

from urllib.request import urlopen, Request

def upload_commonnames(input_path, payload, address):

    with open(input_path) as fileobject:
        with open(input_path + ".2", 'w') as f2:
            f2.write("mapping = {\n")
            for line in fileobject:

                m = re.search("\"(.*)\"[^:]*[:][^:]*\"(.*)\"", line)

                if m is not None:
                    payload += ("\n"+m.group(1)+" "+m.group(2))
                    f2.write("    \"%s\" : \"%s\",\n" % (m.group(1), m.group(2)))
            f2.write("}\n")

    req = Request(address + "/commonnames", payload.encode('utf-8'), {"Content-type": "text/plain"})
    f = urlopen(req)

    print(f.read())

def prepare_payosha(password, address):

    x = "payosha256:create_operation:" + password

    payload  = x + ":post_apgsearch_haul:00008\n"
    payload += x + ":verify_apgsearch_haul:001\n"
    payload += x + ":submit_verification:001\n"
    payload += x + ":register_server:000001\n"

    req = Request(address + "/payosha256", payload.encode('utf-8'), {"Content-type": "text/plain"})
    f = urlopen(req)

    print(f.read())

if __name__ == '__main__':

    from sys import argv

    prepare_payosha(argv[1], argv[2])
    upload_commonnames("all-common-names.txt", ("%s OVERWRITE std" % argv[1]), argv[2])
    upload_commonnames("wiki/wiki-names.txt", ("%s UPDATE std" % argv[1]), argv[2])
    upload_commonnames("override-wiki-names.txt", ("%s UPDATE std" % argv[1]), argv[2])
    upload_commonnames("all-badges.txt", ("%s OVERWRITE badges" % argv[1]), argv[2])
    upload_commonnames("all-rules.txt", ("%s OVERWRITE rules" % argv[1]), argv[2])
    upload_commonnames("all-rule-families.txt", ("%s OVERWRITE rulefamilies" % argv[1]), argv[2])
    upload_commonnames("nft-mapping.txt", ("%s OVERWRITE nfts" % argv[1]), argv[2])
    upload_commonnames("reverse-nft-mapping.txt", ("%s OVERWRITE revnfts" % argv[1]), argv[2])

    remaps = [x for x in os.listdir('remaps') if x.endswith('.txt')]

    for fname in remaps:
        rulename = fname.split('.')[0]
        upload_commonnames(os.path.join("remaps", fname), ("%s UPDATE remap_%s" % (argv[1], rulename)), argv[2])
