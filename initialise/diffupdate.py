from urllib.request import urlopen, Request
from sys import argv
import os
from time import sleep

# RLE syntheses for linear-growth patterns (update these, but be careful
# that they're absolutely correct before committing because there are no
# validity checks):
always_include = '''
#CSYNTH yl78_1_20_ce2bb2c697ad330277935c32ef179035 costs 14 gliders (true).
x = 74, y = 80, rule = B3/S23
66bobo$66b2o$67bo14$39bo11bobo$39bobo9b2o$39b2o11bo3$54b3o$54bo$55bo7$
51bo$49b2o$50b2o4$23bo$24bo$22b3o46b3o$bo69bo$2bo69bo$3o46b3o$49bo$50b
o4$22b2o$23b2o$22bo7$18bo$19bo$17b3o3$21bo11b2o$21b2o9bobo$20bobo11bo
14$6bo$6b2o$5bobo!

#CSYNTH yl1344_2_1118_c2aaaf958f8f412c06554ab3e40b8749 costs 6 gliders (true).
x = 23, y = 23, rule = B3/S23
9bobo$10b2o$10bo6bo$16bo$16b3o$20b2o$20bobo$20bo2$o$b2o$2o5$3b2o$2bob
o$4bo2$5b3o$5bo$6bo!
'''

def partial_consistency_check(min_paths, apgcodes, exhausted):
    '''
    Runs a consistency check against only the components involved in
    computing a specific set of objects.
    '''

    from shinjuku.checks import check_single_line

    to_check = []

    for cstr in apgcodes:
        curr = cstr

        if cstr not in min_paths:
            continue

        while curr not in exhausted:
            pred = min_paths[curr]
            to_check.append(pred[2])
            exhausted.add(curr)
            curr = pred[1]

    print('Checking %d components...' % len(to_check))
    for line in to_check:
        check_single_line(line)


def parse_csv(res):

    return [tuple(s.replace('"', '').split(',')) for s in res.split('\n') if ',' in s][1:]


def get_knowns(address, max_errors=8):

    summary = urlopen(address + '/textcensus/b3s23/synthesis-costs/summary').read().decode()

    tabs = [x for x in summary.split('\n') if ' tabulation ' in x]
    tabs = [x.split(' tabulation ')[-1].strip() for x in tabs]
    tabs = [x for x in tabs if x.startswith('x')]

    knowns = [x for x in parse_csv(summary) if x[0].startswith('x')]
    print('Initial length: %d' % len(knowns))

    i = 0
    j = 0
    gm = 0.5 + 1.25 ** 0.5

    while (i < len(tabs)):

        t = tabs[i]
        print('Downloading %s...' % t)

        try:
            res = urlopen(address + '/textcensus/b3s23/synthesis-costs/' + t).read().decode()
            knowns += parse_csv(res)
        except Exception as e:
            print(e)
            sleep(gm ** j) # exponential backoff
            tabs.append(t)
            j += 1

        if (j > max_errors):
            raise ValueError("%d errors have occurred; terminating..." % j)

        i += 1

    print('Final length: %d' % len(knowns))
    return knowns


def main():

    address = argv[2]

    knowns = get_knowns(address)
    supposed_pseudos = set([k for (k, v) in knowns if int(v) < 100000000000000000])
    knowns = {k : (int(v) % 100000000000000000) for (k, v) in knowns}

    # uncomment this to refresh all syntheses on Catagolue:
    # knowns = {'xp15_4r4z4r4': 3}

    from shinjuku.search import dijkstra, lookup_synth, lt

    min_paths = dijkstra(prefer_long=True)

    coapgcodes = [k for (k, v) in knowns.items() if (v < 1000000) and ((k not in min_paths) or (min_paths[k][0] > v))]

    if len(coapgcodes) > 0:
        print("%d apgcodes with better syntheses in Catagolue: %s" % (len(coapgcodes), coapgcodes))

        with open('cata.txt', 'w') as f:
            for x in coapgcodes:
                f.write(x + '\n')

    apgcodes = [k for (k, v) in min_paths.items() if ((len(k) > 0) and ((k not in knowns) or (knowns[k] > v[0])))]

    if ('xp15_4r4z4r4' in apgcodes):
        raise ValueError("No pentadecathlon found online!")

    with open('false_pseudos.txt') as f:
        for line in f:
            line = line.strip()
            if line in supposed_pseudos:
                apgcodes.append(line)

    apgcodes.append('xp15_4r4z4r4')
    # apgcodes.append('xq30_yq232x232zug1hmgc865da808ad568cgmh1guybug1hmgc865da808ad568cgmh1guz124w6yb6w421yb124w6yb6w421')
    apgcodes = sorted(set([x for x in apgcodes if x.startswith('x') and len(x) <= 1280]))

    print("%d total objects found" % len(min_paths))
    print("%d can be updated" % len(apgcodes))

    if (len(apgcodes) < 10000):
        print("Preparing to update %s" % apgcodes)

    with open("stdin.txt", 'w') as f:
        for x in apgcodes:
            if x[:2] in {'xs', 'xp', 'xq'}:
                f.write(x + '\n')

    print("Written apgcodes")

    os.system("cat stdin.txt | ./apgluxe -L 1 -t 1 -n 10000000")

    logfiles = [x for x in os.listdir() if (len(x) > 5) and (x[:4] == 'log.')]

    trues = []

    for logfile in logfiles:

        print('Log file: %s' % logfile)

        with open(logfile) as f:
            trues += list(f.read().split())

    trues = set([l for l in trues if (len(l) > 0) and (l[0] == 'x') and (l in min_paths)])

    print('%d objects are true.' % len(trues))

    with open('include_these.txt') as f:
        for line in f:
            line = line.strip()
            if (line not in min_paths) and (line not in knowns):
                apgcodes.append(line)

    del knowns

    # Sorting the apgcodes reduces datastore costs by ensuring that each
    # batch involves as few tabulations as possible:
    apgcodes.sort()

    max_segment_size = 800

    irrevocable_errors = 0

    exhausted = {""}

    while (len(apgcodes) > 0):

        segment_size = min(max_segment_size, len(apgcodes))
        segment = apgcodes[:segment_size]
        apgcodes = apgcodes[segment_size:]

        partial_consistency_check(min_paths, segment, exhausted)

        payload = "%s SYNTH b3s23\n" % argv[1]
        rles = []

        if (len(apgcodes) == 0):

            rles.append(always_include)

        print('Creating payload...')

        for apgcode in segment:

            if apgcode == 'xs0_0':
                pass
            elif apgcode in min_paths:
                N, pat = lookup_synth(min_paths, apgcode)
                rle = '\n#CSYNTH %s costs %d gliders (%s).\n' % (apgcode, N, ('true' if (apgcode in trues) else 'pseudo'))
                rle += pat.rle_string()
                rles.append(rle)
            else:
                rles.append('\n#CSYNTH %s costs 999999999999999999 gliders.\n' % apgcode)

        payload += '\n'.join(rles)

        result = 'Unknown error encountered'

        print('Sending payload...')

        try:
            req = Request(address + "/commonnames", payload.encode('utf-8'), {"Content-type": "text/plain"})
            f = urlopen(req)
            result = str(f.read())
        except:
            pass

        print(result)

        if 'success' not in result:
            sleep(1)
            if (max_segment_size >= 100):
                max_segment_size = max_segment_size >> 1
                print("Reducing maximum segment size to %d" % max_segment_size)
                apgcodes += segment
            else:
                print("Carry on regardless")
                irrevocable_errors += 1

        print('%d apgcodes remaining' % len(apgcodes))

    if irrevocable_errors:
        raise ValueError("Some irrevocable errors occurred")

if __name__ == '__main__':

    main()
