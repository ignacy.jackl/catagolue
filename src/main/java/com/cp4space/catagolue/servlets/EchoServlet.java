package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.MalformedURLException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EchoServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String uri = req.getRequestURI();
        if (req.getQueryString() != null) { uri += "?" + req.getQueryString(); }

        uri = "https://conwaylife.com" + uri.substring(uri.indexOf("echo") + 4);

        try {

            URL url = new URL(uri);
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line;

            resp.setContentType("text/plain");
            PrintWriter writer = resp.getWriter();
            while ((line = reader.readLine()) != null) {
                writer.println(line);
            }
            reader.close();

        } catch (FileNotFoundException e) {

            resp.sendError(HttpServletResponse.SC_NOT_FOUND);

        }
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        resp.setContentType("text/plain");

        BufferedReader reader = req.getReader();
        PrintWriter writer = resp.getWriter();
        
        String line = null;
        int i = 0;
        
        writer.println("*** Catagolue echo service received the following data: ***");
        
        while ((line = reader.readLine()) != null) {
            i++;
            writer.println(i + ": " + line);
        }
        
        writer.println("***********************************************************");
    }
}
