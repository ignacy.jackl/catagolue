package com.cp4space.catagolue.census;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;
import java.util.Set;
import java.util.TreeSet;
import java.util.ConcurrentModificationException;

import com.cp4space.catagolue.parmenides.Parmenides;
import com.cp4space.catagolue.servlets.CensusServlet;
import com.cp4space.catagolue.servlets.ObjectServlet;
import com.cp4space.catagolue.servlets.AttributeServlet;
import com.cp4space.catagolue.servlets.UserServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.apphosting.api.ApiProxy;

public class Census {

    private static final Logger log = Logger.getLogger(Census.class.getName());

    public static int JUST_UPLOADED = 0;
    public static int JUST_COMMITTED = 2;

    public static int UNVERIFIED = 1;
    public static int VERIFIED = 3;
    public static int REJECTED = 4;

    public Map<String, Long> table;
    public Map<String, Set<String>> samples;

    public Census() {
        table = new HashMap<String, Long>();
        samples = new HashMap<String, Set<String>>();
    }

    public static void safeSleep(int timeMillis) {
        try {
            log.warning("-- sleeping for " + String.valueOf(timeMillis) + " milliseconds");
            Thread.sleep(timeMillis);
        } catch (InterruptedException e) {
            log.severe("InterruptedException encountered whilst sleeping.");
        }
    }

    public static void multiput(DatastoreService datastore, Iterable<Entity> entities) {
        for (int attempt = 0; attempt < 5; attempt++) {
            try {
                datastore.put(entities);
                break;
            } catch (ConcurrentModificationException e) {
                log.severe("ConcurrentModificationException encountered.");
                safeSleep(1000 << attempt);
            }
        }
    }

    public static void singleput(DatastoreService datastore, Entity entity) {
        List<Entity> entities = new ArrayList<Entity>();
        entities.add(entity);
        multiput(datastore, entities);
    }

    public long totobjs() {
        long to = 0;
        for (Map.Entry<String, Long> entry : table.entrySet()) {
            long quantity = entry.getValue();
            to += quantity;
        }
        return to;
    }

    public static String apgprefix(String apgcode) {

        if (apgcode == null) { return "_"; }
        String prefix = apgcode.split("_")[0];
        if (prefix.length() == 0) { return "_"; }
        return prefix;

    }

    public static String serialiseSortTable(Map<String, Long> map, int limit, boolean swap) {
        String newBoard = "";

        int i = 0;

        for (Map.Entry<String, Long> entry : sortTable(map)) {

            if (i == limit) {
                break;
            }

            String displayedName = entry.getKey();
            long quantity = entry.getValue();

            if (swap) {
                newBoard += displayedName + " " + String.valueOf(quantity) + "\n";
            } else {
                newBoard += String.valueOf(quantity) + " " + displayedName + "\n";
            }

            i++;
        }

        return newBoard;
    }

    public static String serialiseSortTable(Map<String, Long> map) {
        return serialiseSortTable(map, -1, false);
    }

    public static TreeSet<Map.Entry<String, Long>> sortTable(Map<String, Long> map) {

        // Define a custom comparator to sort frequencies in descending order
        // (with objects of the same frequency in lexicographical order):
        Comparator<Map.Entry<String, Long>> comparator = new Comparator<Map.Entry<String, Long>>(){
            @Override
                public int compare(final Map.Entry<String, Long> o1, final Map.Entry<String, Long> o2){
                    int x = -o1.getValue().compareTo(o2.getValue());
                    if (x == 0) {
                        return o1.getKey().compareTo(o2.getKey());
                    } else {
                        return x;
                    }
                }
        };

        TreeSet<Map.Entry<String, Long>> treeSet = new TreeSet<Entry<String, Long>>(comparator);
        treeSet.addAll(map.entrySet());

        return treeSet;

    }

    public TreeSet<Map.Entry<String, Long>> sortedTable() {

        return sortTable(table);
    }

    public static void appendMetadatumLowmem(DatastoreService datastore, String rulestring, String datumName, Map<String, String> mdatumlist) {

        Map<String, String> msublist = new HashMap<String, String>();

        int i = 0;
        int chunkSize = 2000;

        for (Entry<String, String> ms : mdatumlist.entrySet()) {

            msublist.put(ms.getKey(), ms.getValue()); i += 1;

            if (i % chunkSize == 0) {

                log.warning("-- processing objects "+String.valueOf(i - chunkSize + 1)+" to "+String.valueOf(i));
                updateMetadatumParallel(datastore, rulestring, datumName, msublist, true);
                msublist.clear();
                safeSleep(250);
            }

        }

        if (i == 0) { return; }

        log.warning("-- processing objects "+String.valueOf(i - (i % chunkSize) + 1)+" to "+String.valueOf(i));
        updateMetadatumParallel(datastore, rulestring, datumName, msublist, true);

        safeSleep(250);

    }

    public static void updateMetadatumParallel(DatastoreService datastore, String rulestring, String datumName, Map<String, String> mdatumlist, boolean append) {

        Set<Key> setOfKeys = new TreeSet<Key>();

        for (String apgcode : mdatumlist.keySet()) {
            Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();
            setOfKeys.add(datumKey);
        }

        Map<Key, Entity> metadataEntities = new HashMap<Key, Entity>();
        if (append) { metadataEntities = datastore.get(setOfKeys); }

        for (String apgcode : mdatumlist.keySet()) {
            Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();
            if (metadataEntities.get(datumKey) == null) {
                Entity metadataEntity = new Entity(datumKey);
                String newData = mdatumlist.get(apgcode);
                if ((newData != null) && (newData.length() >= 1)) {
                    byte[] comp = GzipUtil.zip(newData, "gz");
                    metadataEntity.setUnindexedProperty("compressed", true);
                    metadataEntity.setProperty("data", new Blob(comp));
                    metadataEntities.put(datumKey, metadataEntity);
                }
            } else {
                Entity metadataEntity = metadataEntities.get(datumKey);
                String oldData = Tabulation.getArbitraryData(metadataEntity, "data");
                if ((oldData.length() > 0) && (oldData.charAt(oldData.length() - 1) != '\n')) { oldData += "\n"; }
                byte[] comp = GzipUtil.zip(oldData + mdatumlist.get(apgcode), "gz");
                metadataEntity.setUnindexedProperty("compressed", true);
                metadataEntity.setProperty("data", new Blob(comp));
            }
        }

        multiput(datastore, metadataEntities.values());

    }

    public static String getMetadatum(DatastoreService datastore, String rulestring, String apgcode, String datumName) {
        Key datumKey = new KeyFactory.Builder("Rule", rulestring).addChild("Object", apgcode).addChild("Metadatum", datumName).getKey();

        try {
            Entity metadataEntity = datastore.get(datumKey);
            String oldData = Tabulation.getArbitraryData(metadataEntity, "data");
            return oldData;
        } catch (EntityNotFoundException e) {
            return null;
        }       
    }

    public static void logLine(PrintWriter writer, String message) {

        writer.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z").format(new Date()) + " -- " + message);
        log.info(message);

    }

    public static void updateLeaderboard(DatastoreService datastore, List<Entity> hauls, Key boardKey, String property) {

        Entity leaderboard;
        Map<String, Long> scoremap = new HashMap<String, Long>();

        try {
            leaderboard = datastore.get(boardKey);

            String[] lines = ((Text) leaderboard.getProperty("data")).getValue().split("\n");

            for (String line : lines) {

                if (line != null && line.length() >= 1) {

                    String[] parts = line.split(" ");

                    String displayedName;
                    long quantity;

                    if (parts[0].matches("[0-9]*")) {
                        displayedName = parts[1];
                        for (int i = 2; i < parts.length; i++) {
                            displayedName += " " + parts[i];
                        }
                        quantity = Long.valueOf(parts[0]);
                    } else {
                        displayedName = parts[0];
                        for (int i = 1; i < parts.length - 1; i++) {
                            displayedName += " " + parts[i];
                        }
                        quantity = Long.valueOf(parts[parts.length - 1]);                       
                    }

                    if (scoremap.containsKey(displayedName)) {
                        scoremap.put(displayedName, scoremap.get(displayedName) + quantity);
                    } else {
                        scoremap.put(displayedName, quantity);
                    }
                }
            }

        } catch (EntityNotFoundException e) {
            leaderboard = new Entity(boardKey);
        }

        for (Entity haul : hauls) {
            String displayedName = (String) haul.getProperty("displayedName");
            displayedName = displayedName.replace("+", "#").split("#")[0];
            long quantity = (long) haul.getProperty(property);

            if (scoremap.containsKey(displayedName)) {
                scoremap.put(displayedName, scoremap.get(displayedName) + quantity);
            } else {
                scoremap.put(displayedName, quantity);
            }
        }

        leaderboard.setProperty("data", new Text(serialiseSortTable(scoremap)));

        datastore.put(leaderboard);

    }

    public void backoutHaul(PrintWriter writer, String rulestring, String symmetry, String haulcode, long aq) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", haulcode).getKey();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);
        try {
            Entity haul = datastore.get(haulKey);
            affineAdd(Tabulation.getArbitraryData(haul, "censusData"), -aq);
            updateTabulations(rulestring, symmetry, null, null, false);
            Entity census = datastore.get(censusKey);
            long newobjects = (long) haul.getProperty("numobjects");
            long newsoups = (long) haul.getProperty("numsoups");
            census.setProperty("numsoups", ((long) census.getProperty("numsoups")) - aq * newsoups);
            census.setProperty("numobjects", ((long) census.getProperty("numobjects")) - aq * newobjects);
            datastore.put(census);
            writer.println("Successful.");
        } catch (EntityNotFoundException e) {

        }

    }

    public static boolean isCensusLarge(String rulestring, String symmetry) {

        if (ObjectServlet.isUnofficial(symmetry) >= 2) {
            log.warning("Symmetry is unofficial; omitting Parmenides...");
            return false;
        } else if (!rulestring.matches("b[1-8]*s[0-8]*")) {
            log.warning("Not an outer-totalistic 2-state non-B0 Moore rule; omitting Parmenides...");
            return false;
        } else if (rulestring.equals("b3s2")) {
            return false;
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        try {
            Entity census = datastore.get(censusKey);
            long censusObjects = (long) census.getProperty("numobjects");
            long oneMillion = 1000000;
            long oneTrillion = oneMillion * oneMillion;
            if (censusObjects >= oneTrillion) {
                log.warning("Census contains > 10^12 objects; using Parmenides...");
                return true;
            } else {
                log.warning("Census contains < 10^12 objects; omitting Parmenides...");
                return false;
            }
        } catch (EntityNotFoundException e) {
            log.warning("Census is completely new; omitting Parmenides...");
            return false;
        }

    }

    public static void fullCronJob(PrintWriter writer, String rulestring, String symmetry) {

        boolean useParmenides = isCensusLarge(rulestring, symmetry.replace("G", "C").replace("H", "D"));
        int limit = 60;
        if (rulestring.equals("b3s23")) { limit = 360; }
        if (rulestring.equals("b45s12")) { limit = 1; }
        if (rulestring.equals("b6s3456")) { limit = 10; }
        if (rulestring.equals("b345s5678")) { limit = 10; }
        if (rulestring.equals("b3458s5678")) { limit = 5; }
        if (rulestring.equals("xdoscillators")) { limit = 2; }

        Census census = new Census();
        census.processHauls(writer, rulestring, symmetry, useParmenides, limit);
        Map<String, String> mdatumlist = census.gatherSamples(rulestring, symmetry);

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        if (mdatumlist.size() > 0) {
            logLine(writer, "Writing sample soups for "+String.valueOf(mdatumlist.size())+" objects.");
            appendMetadatumLowmem(datastore, rulestring, "samples", mdatumlist);
            logLine(writer, "Sample soups saved in Catagolue metadata.");
        }

        if (useParmenides) {
            runParmenides(writer, rulestring, symmetry, limit);
        }

        if (rulestring.equals("b3s23") && ObjectServlet.isOfficialSquare(symmetry)) {

            logLine(writer, "Attributing objects...");

            safeSleep(1000);

            for (String apgcode : mdatumlist.keySet()) {

                // still-lifes and oscillators with period <= 3 and long-lived soups:
                boolean unnotable = (apgcode.charAt(1) == 's') || ((apgcode.charAt(1) == 'p') && (apgcode.charAt(3) == '_') && (apgcode.charAt(2) <= '3')) || (apgcode.charAt(0) == 'm');
                String attrsym = "Symmetric";

                if (symmetry.equals("C1") || symmetry.equals("G1")) {
                    // still-lifes with between 18 and 29 bits are unnotable:
                    unnotable = (apgcode.charAt(1) == 's') && (apgcode.charAt(4) == '_') && ((apgcode.charAt(2) == '2') || ((apgcode.charAt(2) == '1') && (apgcode.charAt(1) >= '8')));
                    attrsym = "C1";
                }

                if (unnotable) {
                    writer.println(apgcode + " is deemed insufficiently interesting.");
                } else {
                    log.warning(" -- attributing object " + apgcode);
                    AttributeServlet.attributeObject(apgcode, rulestring, attrsym, datastore, writer);
                }
            }

            logLine(writer, "...objects attributed.");
        }

        deleteHauls(writer, rulestring, symmetry);

    }

    public void processHauls(PrintWriter writer, String rulestring, String symmetry, boolean useParmenides, int limit) {

        log.warning("Processing hauls for "+rulestring+"/"+symmetry+"...");

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        List<Entity> hauls = new ArrayList<Entity>();
        boolean fullQueue = false;

        for (int i = 0; i < (useParmenides ? 1 : 2); i++) {

            Filter propertyFilter = new FilterPredicate("committed",
                    FilterOperator.EQUAL,
                    ((i == 0) ? VERIFIED : JUST_UPLOADED));
            Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
            PreparedQuery preparedQuery = datastore.prepare(query);

            hauls.addAll(preparedQuery.asList(FetchOptions.Builder.withLimit(limit - hauls.size())));

            if (hauls.size() == limit) { fullQueue = true; break; }
        }

        if (hauls.size() == 0) {
            logLine(writer, "No hauls found.");
            try {
            Entity census = datastore.get(censusKey);
            census.setProperty("uncommittedHauls", 0);
            datastore.put(census);
            } catch (EntityNotFoundException e) { }
            return;
        }

        logLine(writer, hauls.size() + " hauls found.");
        log.warning(hauls.size() + " hauls found.");

        long newobjects = 0;
        long newsoups = 0;

        // Load the hauls into the census:
        for (Entity haul : hauls) {

            String censusDataString = Tabulation.getArbitraryData(haul, "censusData");

            if (rulestring.equals("b3s23") && (symmetry.equals("C1") || symmetry.equals("G1"))) {
                String displayedName = (String) haul.getProperty("displayedName");
                if (censusDataString.contains("xp2_882030kgz010602 ")) { UserServlet.createBadge(datastore, displayedName, "Conchita"); }
                if (censusDataString.contains("xp8_4b23021eaz57840c4d2 ")) { UserServlet.createBadge(datastore, displayedName, "Hitchhiker"); }
            }

            affineAdd(censusDataString, 1);
            newobjects += (long) haul.getProperty("numobjects");
            newsoups += (long) haul.getProperty("numsoups");
            // Mark the haul as committed:
            haul.setProperty("committed", JUST_COMMITTED);
        }

        logLine(writer, "Hauls loaded into census.");

        // Update the committed status of the hauls in the datastore:
        multiput(datastore, hauls);
        logLine(writer, "Hauls marked as committed.");

        // Remap pseudo-objects:
        Map<String, String> remap = new HashMap<>();
        CommonNames.getNamemap("remap_" + rulestring, remap, false);
        getdiff(table, remap, table);

        // Add the census to the existing tabulations:
        Census diffcensus = new Census();
        Map<String, Boolean> isTooLarge = updateTabulations(rulestring, symmetry, remap, diffcensus, false);

        // Update census with remapping:
        if (diffcensus.table.size() > 0) {
            safeSleep(1200);
            diffcensus.updateTabulations(rulestring, symmetry, null, null, false);
        }

        logLine(writer, "Tabulations updated.");

        // Update (or, if necessary, create) the parent census:
        // Do this really quickly to make it practically atomic:
        try {
            Entity census = datastore.get(censusKey);
            census.setProperty("uncommittedHauls", (fullQueue ? 1 : 0));
            census.setProperty("committedHauls", ((long) census.getProperty("committedHauls")) + hauls.size());
            census.setProperty("lastModified", new Date());
            census.setProperty("numsoups", ((long) census.getProperty("numsoups")) + newsoups);
            census.setProperty("numobjects", ((long) census.getProperty("numobjects")) + newobjects);
            datastore.put(census);
        } catch (EntityNotFoundException e) {
            Entity census = new Entity("Census", rulestring + "/" + symmetry);
            census.setProperty("uncommittedHauls", 0);
            census.setProperty("committedHauls", hauls.size());
            census.setProperty("symmetries", symmetry);
            census.setProperty("rulestring", rulestring);
            census.setProperty("numsoups", newsoups);
            census.setProperty("numobjects", newobjects);
            census.setProperty("lastModified", new Date());
            datastore.put(census);
        }

        logLine(writer, "Census updated.");

        // Update leaderboard:
        updateLeaderboard(datastore, hauls, new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Leaderboard", "numobjects").getKey(), "numobjects");

        logLine(writer, "Leaderboard updated.");

        if (symmetry.length() > 50) {

            logLine(writer, "Skipping sample soups owing to long symmetry name.");

        } else {

            // Load sample soups:
            for (Entity haul : hauls) {
                includeSamples(
                        Tabulation.getArbitraryData(haul, "soupData"),
                        symmetry, (String) haul.getProperty("root"),
                        (rulestring.equals("b3s23") ? null : isTooLarge));
            }

            logLine(writer, "Sample soups loaded.");

        }

    }

    public Map<String, String> gatherSamples(String rulestring, String symmetry) {

        Map<String, String> mdatumlist = new HashMap<String, String>();

        if (samples.size() > 0) {

            // Save the sample soups in the Catagolue:

            int inclimit = (symmetry.equals("C1") ? 500 : 200);
            if (ObjectServlet.isUnofficial(symmetry) >= 2) { inclimit = 100; }

            for (String apgcode : samples.keySet()) {
                if (apgcode != null && table.containsKey(apgcode) && (table.get(apgcode) <= inclimit)) {

                    String prefix = apgprefix(apgcode);

                    StringBuilder mdatumbuilder = new StringBuilder();

                    for (String line : samples.get(apgcode)) {
                        mdatumbuilder.append(line + "\n");
                    }
                    samples.get(apgcode).clear();

                    mdatumlist.put(apgcode, mdatumbuilder.toString());
                }
            }
        }

        // Liberate memory:
        samples.clear();
        table.clear();

        return mdatumlist;

    }

    public static void deleteHauls(PrintWriter writer, String rulestring, String symmetry) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        int hoffset = (rulestring.equals("b3s23") ? (symmetry.equals("C1") ? 10000 : 5000) : 100);

        Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, 2);
        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query.setKeysOnly());
        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withDefaults().offset(hoffset).limit(200));

        if (hauls.size() > 0) {
            logLine(writer, "Deleting " + hauls.size() + " hauls...");
            List<Key> itemsToDelete = new ArrayList<Key>();
            for (Entity haul : hauls) { itemsToDelete.add(haul.getKey()); }
            datastore.delete(itemsToDelete);
        }

    }

    public static void runParmenides(PrintWriter writer, String rulestring, String symmetry, int limit) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        logLine(writer, "Running Parmenides...");

        Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, JUST_UPLOADED);
        Query query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);

        List<Entity> hauls = preparedQuery.asList(FetchOptions.Builder.withLimit(limit));

        logLine(writer, hauls.size() + " hauls found.");

        if (hauls.size() > 0) {

            double censusRatio = -1.0;

            try {
                Entity census = datastore.get(censusKey);
                long censusSoups = ((long) census.getProperty("numsoups"));
                long censusObjects = ((long) census.getProperty("numobjects"));
                censusRatio = ((double) censusObjects) / ((double) censusSoups);
            } catch (EntityNotFoundException e) {
            }

            Entity cachedCensus = Parmenides.retrieveTable(rulestring, symmetry);

            for (Entity haul : hauls) {

                Census haulCensus = new Census();
                String root = (String) haul.getProperty("root");
                haulCensus.affineAdd(Tabulation.getArbitraryData(haul, "censusData"), 1);
                haulCensus.includeSamples(Tabulation.getArbitraryData(haul, "soupData"), symmetry, root, null, true);
                long claimedobjs = (long) haul.getProperty("numobjects");
                long claimedsoups = (long) haul.getProperty("numsoups");
                double chisquaretotal = Parmenides.chisquare(cachedCensus, haulCensus, claimedobjs);

                if (censusRatio > 0.0) {
                    double haulRatio = ((double) claimedobjs) / ((double) claimedsoups);
                    double hrcr = haulRatio / censusRatio;
                    if ((hrcr > 2.5) || (hrcr < 0.4)) { chisquaretotal += 1e9; }
                }

                writer.println(root+": "+String.valueOf(chisquaretotal));

                haul.setProperty("committed", UNVERIFIED);

                StringBuilder privateData = new StringBuilder();
                StringBuilder publicData = new StringBuilder();

                Parmenides.rareObjects(rulestring, cachedCensus, haulCensus, privateData, publicData);

                haul.setUnindexedProperty("chisquare", chisquaretotal);

                haul.setUnindexedProperty("compressed", true);
                byte[] comp1 = GzipUtil.zip(privateData.toString(), "gz");
                haul.setProperty("privateData", new Blob(comp1));
                byte[] comp2 = GzipUtil.zip(publicData.toString(), "gz");
                haul.setProperty("publicData", new Blob(comp2));

            }

            multiput(datastore, hauls);
        }

        logLine(writer, "...Parmenides completed!");
    }

    public static void getdiff(Map<String, Long> tcomb, Map<String, String> tdecomp, Map<String, Long> diffmap) {

        // Use a temporary map to avoid ConcurrentModificationException when
        // iterating over the original map (avoids the edge case where tcomb
        // and diffmap are the same map):
        Map<String, Long> output = new HashMap<>();

        for (Map.Entry<String, String> entry : tdecomp.entrySet()) {
            String apgcode = entry.getKey();
            if (tcomb.containsKey(apgcode)) {
                long multiplier = tcomb.get(apgcode);
                String[] mapparts = entry.getValue().replace(" ", "").split(",");
                for (String mappart : mapparts) {
                    long mm = multiplier;
                    String newcode = "";
                    String[] parts2 = mappart.split("[*]");
                    for (String subpart : parts2) {
                        if (subpart.contains("_")) {
                            newcode = subpart;
                        } else {
                            mm *= Long.valueOf(subpart);
                        }
                    }
                    if (output.containsKey(newcode)) {
                        output.put(newcode, mm + output.get(newcode));
                    } else {
                        output.put(newcode, mm);
                    }
                }
                if (output.containsKey(apgcode)) {
                    output.put(apgcode, output.get(apgcode) - multiplier);
                } else {
                    output.put(apgcode, -multiplier);
                }
            }
        }

        for (Map.Entry<String, Long> entry : output.entrySet()) {
            String apgcode = entry.getKey();
            long quantity = entry.getValue();
            if (quantity == 0) { continue; }
            if (diffmap.containsKey(apgcode)) {
                diffmap.put(apgcode, quantity + diffmap.get(apgcode));
            } else {
                diffmap.put(apgcode, quantity);
            }
        }
    }

    public static void unionCensuses(String rulestring, String[] symmetries, String newsym) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        TreeSet<String> prefices = new TreeSet<String>();
        long totsoups = 0;

        for (String symmetry : symmetries) {

            Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

            try {
                Entity census = datastore.get(censusKey);
                totsoups += ((long) census.getProperty("numsoups"));
            } catch (EntityNotFoundException e) {
            }

            Query query = new Query("Tabulation", censusKey).setKeysOnly();
            PreparedQuery preparedQuery = datastore.prepare(query);
            List<Entity> oldtabs = preparedQuery.asList(FetchOptions.Builder.withLimit(2000));
            for (Entity oldtab : oldtabs) {
                String prefix = oldtab.getKey().getName();
                prefices.add(prefix);
            }
        }

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + newsym);

        for (String prefix : prefices) {
            Map<String, Long> tcomb = new HashMap<String, Long>();
            for (String symmetry : symmetries) {
                try {
                    Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Tabulation", prefix).getKey();
                    Entity tabulation = datastore.get(tabulationKey);
                    String censusData = Tabulation.getCensusData(tabulation);
                    affineAddToMap(censusData, 1, tcomb, "");
                } catch (EntityNotFoundException e) {
                }
            }

            Tabulation tnew = new Tabulation(prefix);

            boolean nonempty = false;

            for (Map.Entry<String, Long> entry : sortTable(tcomb)) {
                String apgcode = entry.getKey();
                long quantity = entry.getValue();

                if (quantity != 0) {
                    nonempty = true;
                    tnew.appendRow(apgcode, quantity);
                }
            }

            if (nonempty) {
                Entity tab = new Entity("Tabulation", prefix, censusKey);
                tnew.writeTab(tab);
                datastore.put(tab);
            }
        }

        Entity census = new Entity("Census", rulestring + "/" + newsym);
        census.setProperty("uncommittedHauls", 0);
        census.setProperty("committedHauls", 0);
        census.setProperty("symmetries", newsym);
        census.setProperty("rulestring", rulestring);
        census.setProperty("numsoups", totsoups);
        census.setProperty("numobjects", 0);
        census.setProperty("lastModified", new Date());
        datastore.put(census);

    }

    /**
     * Load tabulations from the Datastore and update them.
     * @param rulestring  e.g. b3s23
     * @param symmetry  e.g. C1
     */
    public Map<String, Boolean> updateTabulations(String rulestring, String symmetry, Map<String, String> decomp, Census remapped, boolean overwrite) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

        // Obtain set of prefices (each one corresponding to a tabulation):
        TreeSet<String> prefices = new TreeSet<String>();
        for (String apgcode : table.keySet()) {
            prefices.add(apgprefix(apgcode));
        }

        // Include oldest tabulations:
        Query query = new Query("Tabulation", censusKey).addSort("lastModified", Query.SortDirection.ASCENDING).setKeysOnly();
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> oldtabs = preparedQuery.asList(FetchOptions.Builder.withLimit(3));
        for (Entity oldtab : oldtabs) {
            String prefix = oldtab.getKey().getName();
            prefices.add(prefix);
        }

        // Create a map of tabulations:
        Map<String, Map<String, Long>> prefixmap = new HashMap<String, Map<String, Long>>();
        Map<String, Map<String, String>> prefixdecomp = new HashMap<String, Map<String, String>>();
        for (String prefix : prefices) {
            prefixmap.put(prefix, new HashMap<String, Long>());
            prefixdecomp.put(prefix, new HashMap<String, String>());
        }

        // Per-tabulation decompositions
        if (decomp != null) {
            for (Map.Entry<String, String> entry : decomp.entrySet()) {
                String apgcode = entry.getKey();
                String prefix = apgprefix(apgcode);
                if (prefixdecomp.containsKey(prefix)) {
                    prefixdecomp.get(prefix).put(apgcode, entry.getValue());
                }
            }
        }

        // Split census amongst tabulations:
        for (Map.Entry<String, Long> entry : table.entrySet()) {
            String apgcode = entry.getKey();
            long quantity = entry.getValue();
            if (quantity != 0) {
                String prefix = apgprefix(apgcode);
                prefixmap.get(prefix).put(apgcode, quantity);
            }
        }

        Map<String, Boolean> isTooLarge = new HashMap<String, Boolean>();       

        // Modify relevant tabulations:
        for (String prefix : prefices) {

            Map<String, Long> tcomb = prefixmap.get(prefix);

            Map<String, Long> owriters = new HashMap<String, Long>();

            if (overwrite) {
                for (Map.Entry<String, Long> entry : tcomb.entrySet()) {
                    owriters.put(entry.getKey(), entry.getValue());
                }
            }

            Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Tabulation", prefix).getKey();
            try {
                Entity tabulation = datastore.get(tabulationKey);
                String censusData = Tabulation.getCensusData(tabulation);
                affineAddToMap(censusData, 1, tcomb, "");
            } catch (EntityNotFoundException e) {
            }

            for (Map.Entry<String, Long> entry : owriters.entrySet()) {
                tcomb.put(entry.getKey(), entry.getValue());
            }

            Tabulation tnew = new Tabulation(prefix);

            boolean nonempty = false;

            for (Map.Entry<String, Long> entry : sortTable(tcomb)) {
                String apgcode = entry.getKey();
                long quantity = entry.getValue();

                if (table.containsKey(apgcode)) { table.put(apgcode, quantity); }

                if (quantity != 0) {
                    nonempty = true;
                    tnew.appendRow(apgcode, quantity);
                }
            }

            if (nonempty) {
                Entity tab = new Entity("Tabulation", prefix, censusKey);
                int bytesConsumed = tnew.writeTab(tab);
                boolean tooLarge = (rulestring.equals("b3s23") ? false : (bytesConsumed > 500000));
                isTooLarge.put(prefix, tooLarge);
                datastore.put(tab);
            } else {
                datastore.delete(tabulationKey);
            }

            if (remapped != null) {
                Map<String, String> tdecomp = prefixdecomp.get(prefix);
                getdiff(tcomb, tdecomp, remapped.table);
            }
            tcomb.clear();
        }

        return isTooLarge;

    }

    public void includeSamples(String soupData, String symmetry, String root, Map<String, Boolean> isTooLarge) {
        includeSamples(soupData, symmetry, root, isTooLarge, false);
    }

    public void includeSamples(String soupData, String symmetry, String root, Map<String, Boolean> isTooLarge, boolean truncate) {

        String[] lines = soupData.split("\n");

        for (String line : lines) {

            if (line.length() < 3) { continue; }

            String[] parts = line.split(" ");
            String apgcode = parts[0];
            String prefix = apgprefix(apgcode);

            // Don't include sample soups for massive tabulations:
            if (isTooLarge == null || isTooLarge.get(prefix) == null || isTooLarge.get(prefix) == false) {

                if (samples.containsKey(apgcode) == false) {
                    samples.put(apgcode, new TreeSet<String>());
                }

                Set<String> stringSet = samples.get(apgcode);
                String longroot = symmetry + "/" + root;

                for (int i = 1; i < parts.length; i++) {
                    String part = parts[i];
                    if (truncate && (part.length() > 30)) {
                        part = part.substring(0, 30);
                    }
                    stringSet.add(longroot + part);
                }
            }
        }
    }

    public static void affineAddToMap(String censusData, long multiple, Map<String, Long> map, String prefix, boolean disj) {

        String[] lines = censusData.split("\n");

        for (String line : lines) {

            if (line.length() >= 2) {

                String[] parts = line.split(" ");

                try {
                    if (parts.length >= 2) {

                        String apgcode = prefix + parts[0];
                        long quantity = Long.valueOf(parts[1]);

                        if (map.containsKey(apgcode)) {
                            map.put(apgcode, map.get(apgcode) + multiple * quantity);
                        } else if (disj) {
                            map.put(apgcode, multiple * quantity);
                        }
                    }
                } catch (NumberFormatException e) {
                    // To stop saboteurs from crashing Catagolue by posting invalid data.
                }
            }           
        }       
    }

    public static void affineAddToMap(String censusData, long multiple, Map<String, Long> map, String prefix) {
        affineAddToMap(censusData, multiple, map, prefix, true);
    }

    public void affineAdd(String censusData, long multiple, boolean disj) {
        affineAddToMap(censusData, multiple, table, "", disj);
    }

    public void removeKeys(Map<String, Long> map) {
        for (Map.Entry<String, Long> entry : map.entrySet()) {
            table.remove(entry.getKey());
        }
    }

    public void affineAdd(String censusData, long multiple) {
        affineAdd(censusData, multiple, true);
    }
}
