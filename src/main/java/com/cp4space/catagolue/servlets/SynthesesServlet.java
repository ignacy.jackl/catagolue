package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.GzipUtil;
import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;

public class SynthesesServlet extends ExampleServlet {

    @Override
    public String getTitle(HttpServletRequest req) {
        return "Syntheses";
    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        writer.println("<p>A <b><a href=\"https://conwaylife.com/wiki/Synthesis\">glider synthesis</a></b> "
                        + "is a recipe for constructing an object by colliding multiple copies of the "
                        + "<i>glider</i>, the simplest moving configuration in the rule B3/S23. The "
                        + "<i>cost</i> of a synthesis is the number of gliders required.</p>");

        writer.println("<h3>Explore known syntheses</h3>");

        writer.println("<p>Catagolue contains a collection of the best (lowest-cost) "
                + "known glider syntheses of patterns, assembled from Jeremy Tan's "
                + "<a href=\"https://gitlab.com/parclytaxel/Shinjuku\">Shinjuku</a> "
                + "database of synthesis components.</p>\n<p>The collection of synthesisable "
                + "objects, which includes all strict still-lifes of 20 or fewer live cells, "
                + "is arranged below:</p>");

        CensusServlet.retrieveContent(writer, "b3s23", "synthesis-costs", false, 0);

        writer.println("<p>As of September 2019, this includes syntheses of all strict still-lifes "
                + "of 17 or fewer cells with strictly less than one glider per cell.</p>");

        writer.println("<h3>Submitting your own syntheses</h3>");

        writer.println("<p>The following box accepts synthesis RLEs conforming to the following requirements:</p><ul>");

        writer.println("<li>The RLE <b>must be two-state</b> (plain B3/S23);</li>");
        writer.println("<li>Pattern cannot exceed 9999x9999, 100000 live cells, or 400 kB;</li>");
        writer.println("<li>Distinct components in the pattern <b>must be separated by at least 20 blank columns or rows</b>;</li>");
        writer.println("<li>Every glider in a component must have started interacting before time 1000, " +
                        "and finished interacting before time 4000.</li>");

        writer.println("</ul><p>Any number of syntheses may be included in the same pattern.</p>");

        writeSynthesisBox(writer, req);

        writer.println("<p>It may take up to 8 hours before your submitted synthesis appears in Catagolue; " +
                        "the update process runs at 04:20, 12:20, and 20:20 UTC each day.</p>");

        writer.println("<p>For advice on finding glider syntheses, consult " +
                        "<a href=\"https://conwaylife.com/wiki/Tutorials/Glider_syntheses\">the tutorial</a>.</p>");
    }
}
