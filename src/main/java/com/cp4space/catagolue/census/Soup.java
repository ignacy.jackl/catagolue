package com.cp4space.catagolue.census;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Soup {
    
    public static byte[][] hashSoup(String prehash, String symmetry) {
        
        // Container for the soup:
        byte[][] theArray = new byte[32][32];
        
        // Assume no symmetry unless otherwise specified:
        String sym = symmetry;
        if (sym == null) {
            sym = "C1";
        }
        
        // Number of lines of diagonal symmetry:
        int d = 0;
        if (sym.equals("D2_x") || sym.equals("D2_xo") || sym.equals("D8_1") || sym.equals("D8_4")) {
            d = 1;
        } else if (sym.equals("D4_x1") || sym.equals("D4_x4")) {
            d = 2;
        }

        try {
            
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(prehash.getBytes("UTF-8"));
            byte[] digest = md.digest();

            MessageDigest md2 = MessageDigest.getInstance("SHA-256");
            md2.update(("hackme" + prehash).getBytes("UTF-8"));
            byte[] digest2 = md2.digest();
            
            // Dump the soup into the array:
            for (int j = 0; j < 32; j++) {
                int t = digest[j];
                
                if (sym.equals("25pct")) {
                    t = t & digest2[j];
                } else if (sym.equals("75pct")) {
                    t = t | digest2[j];
                }
                
                for (int k = 0; k < 8; k++) {
                    
                    int x;
                    int y;
                    
                    if (sym.equals("old8x32")) {
                        x = k + 8*(j % 4) - 16;
                        y = j / 4;
                    } else {
                        x = k + 8*(j % 2);
                        y = j / 2;
                    }
                    
                    if ((t & (1 << (7 - k))) > 0) {
                        if ((d == 0) || (x >= y)) {
                            theArray[x + 16][y + 16] = 1;
                        } else if (sym.equals("D4_x1")) {
                            theArray[y + 16][16 - x] = 1;
                        } else if (sym.equals("D4_x4")) {
                            theArray[y + 16][15 - x] = 1;
                        }
                        
                        if (sym.equals("D4_x1") && (x == y)) {
                            theArray[y + 16][16 - x] = 1;
                        } else if (sym.equals("D4_x4") && (x == y)) {
                            theArray[y + 16][15 - x] = 1;
                        }
                    }
                }
            }
            
            // Diagonal symmetry:
            if (d >= 1) {
                for (int x = 0; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[y][x] = 1;
                        }
                    }
                }
                if (sym.equals("D2_xo")) {
                    for (int x = 16; x < 32; x++) {
                        for (int y = 16; y < 24; y++) {
                            theArray[x][y] ^= theArray[x][47 - y];
                            theArray[x][47 - y] ^= theArray[x][y];
                            theArray[x][y] ^= theArray[x][47 - y];
                        }
                    }
                }
                if (d == 2) {
                    if (sym.equals("D4_x1")) {
                        for (int x = 1; x < 32; x++) {
                            for (int y = 1; y < 32; y++) {
                                if (theArray[x][y] > 0) {
                                    theArray[32-y][32-x] = 1;
                                }
                            }
                        }
                    } else {
                        for (int x = 0; x < 32; x++) {
                            for (int y = 0; y < 32; y++) {
                                if (theArray[x][y] > 0) {
                                    theArray[31-y][31-x] = 1;
                                }
                            }
                        }
                    }
                }
            }
            
            // Vertical symmetry:
            if (sym.equals("D2_+1") || sym.equals("D4_+1") || sym.equals("D4_+2")) {
                for (int x = 0; x < 32; x++) {
                    for (int y = 1; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[x][32-y] = 1;
                        }
                    }
                }
            } else if (sym.equals("D2_+2") || sym.equals("D4_+4")) {
                for (int x = 0; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[x][31-y] = 1;
                        }
                    }
                }
            }

            // Horizontal symmetry:
            if (sym.equals("D4_+1")) {
                for (int x = 1; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[32-x][y] = 1;
                        }
                    }
                }
            } else if (sym.equals("D4_+2") || sym.equals("D4_+4")) {
                for (int x = 0; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[31-x][y] = 1;
                        }
                    }
                }
            }
            
            // Rotate2 symmetry:
            if (sym.equals("C2_1") || sym.equals("C4_1") || sym.equals("D8_1")) {
                for (int x = 1; x < 32; x++) {
                    for (int y = 1; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[32-x][32-y] = 1;
                        }
                    }
                }
            } else if (sym.equals("C2_2")) {
                boolean luxe = (prehash.length() >= 3) && (prehash.substring(0, 2).equals("l_"));
                for (int x = 1; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            if (luxe) {
                                theArray[31-x][32-y] = 1;
                            } else {
                                theArray[32-x][31-y] = 1;
                            }
                        }
                    }
                }
            } else if (sym.equals("C2_4") || sym.equals("C4_4") || sym.equals("D8_4")) {
                for (int x = 0; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[31-x][31-y] = 1;
                        }
                    }
                }
            }
            
            // Rotate4 symmetry:
            if (sym.equals("C4_1") || sym.equals("D8_1")) {
                for (int x = 1; x < 32; x++) {
                    for (int y = 1; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[y][32-x] = 1;
                        }
                    }
                }
            } else if (sym.equals("C4_4") || sym.equals("D8_4")) {
                for (int x = 0; x < 32; x++) {
                    for (int y = 0; y < 32; y++) {
                        if (theArray[x][y] > 0) {
                            theArray[y][31-x] = 1;
                        }
                    }
                }
            }
            
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        
        return theArray;
        
    }

}
