package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.patterns.GolObject;
import com.cp4space.catagolue.servlets.ObjectServlet;
import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;


public class AttributeServlet extends HttpServlet {
    
    static Entity getHaulEntity(String seedroot, String rulestring, String symmetry, DatastoreService datastore) {
        
        Entity haul = null;
        
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(seedroot.getBytes("UTF-8"));
            String haulcode = PayoshaUtils.bytesToHex(md.digest());
            
            Key haulKey = new KeyFactory.Builder("Census", rulestring+"/"+symmetry).addChild("Haul", haulcode).getKey();
            
            haul = datastore.get(haulKey);
            haul.setProperty("committed", 6);
            datastore.put(haul);
            
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (EntityNotFoundException e) {

        }
        
        return haul;
    }
    
    static String getOwner(String seedroot, String rulestring, String symmetry, DatastoreService datastore) {
        
        String owner = null;
        Entity haul = getHaulEntity(seedroot, rulestring, symmetry, datastore);

        if (haul != null) {
            owner = (String) haul.getProperty("displayedName");
        }

        return owner;

    }

    public static void attributeObject(String apgcode, String rulestring, String ysymmetry, DatastoreService datastore, PrintWriter writer) {

        String xsymmetry = ysymmetry.replace("H", "D").replace("G", "C");
        
        writer.println("*** " + apgcode + " ***");

        String occurrences = Census.getMetadatum(datastore, rulestring, apgcode, "samples");
        
        if (occurrences == null || occurrences.length() <= 1) {
            writer.println("There are no sample soups stored in the Catagolue.");
        } else {
            String[] parts = occurrences.split("\n");
            
            int elapsed = 0;
            
            for (String part : parts) {

                if (elapsed >= 20) { break; }

                String symmetry = part.split("/")[0];

                boolean symmetricSoup = ObjectServlet.isOfficialSquare(symmetry);
                if (symmetry.equals("C1") || symmetry.equals("G1")) { symmetricSoup = false; }

                if (symmetry.replace("H", "D").replace("G", "C").equals(xsymmetry) || (xsymmetry.equals("Symmetric") && symmetricSoup)) {

                    String prehash = part.split("/")[1];
                    
                    elapsed += 1;
                    
                    Entity haul = null;
                    String owner = null;
                    String ownerdate = null;

                    int[] lengths = {14, 12, 48};
                    for (int prelength : lengths) {
                        if ((haul == null) && (prehash.length() > prelength)) {
                            haul = getHaulEntity(prehash.substring(0, prelength), rulestring, symmetry, datastore);
                        }
                    }

                    if (haul != null) {
                        owner = (String) haul.getProperty("displayedName");
                        ownerdate = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z").format((Date) haul.getProperty("date"));
                    }

                    if ((owner != null) && (!owner.equals("null"))) {
                        if (CensusServlet.isCensored(owner)) { owner = "Anonymous"; }
                        writer.println("Soup " + prehash + " was found on " + ownerdate + " and is owned by " + owner);
                        if (rulestring.equals("b3s23") && ((xsymmetry.equals("C1") || xsymmetry.equals("Symmetric")))) {

                            String keystring = String.valueOf(elapsed)+"-"+apgcode;

                            if (xsymmetry.equals("Symmetric")) {
                                keystring += "-sym";
                                owner += "#sym";
                            }

                            Key attributionKey = KeyFactory.createKey("Attribution", keystring);
                            try {
                                Entity attribution = datastore.get(attributionKey);
                                // writer.println("Already present in Datastore.");
                            } catch (EntityNotFoundException e) {
                                Entity attribution = new Entity(attributionKey);
                                attribution.setProperty("ordinal", elapsed);
                                attribution.setProperty("apgcode", apgcode);
                                attribution.setProperty("owner", owner);
                                datastore.put(attribution);
                            }
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException {
        
        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();
        
        String apgcode = req.getParameter("apgcode");
        String rulestring = "b3s23";
        String symmetry = "C1";
        String pathinfo = req.getPathInfo();
        
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (apgcode == null)) {
                apgcode = pathParts[1];
            }
            if ((pathParts.length >= 3)) {
                rulestring = pathParts[2];
            }
            if ((pathParts.length >= 4)) {
                symmetry = pathParts[3];
            }
        }
        
        if (apgcode == null) {
            writer.println("Please provide a valid apgcode.");
        } else {
            
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            
            String[] parts = apgcode.split("_");
            
            if (parts.length >= 2) {
                attributeObject(apgcode, rulestring, symmetry, datastore, writer);
            } else {
                writer.println("This is not an apgcode.");
                
                try {

                    Key tabulationKey = new KeyFactory.Builder("Census", rulestring+"/"+symmetry).addChild("Tabulation", apgcode).getKey();
                    Entity tabulation = datastore.get(tabulationKey);
                    Census census = new Census();
                    
                    census.affineAdd(Tabulation.getCensusData(tabulation), 1);

                    TreeSet<Entry<String,Long>> sortedTable = census.sortedTable();
                    
                    for (Entry<String, Long> entry : sortedTable) {
                        String apgcode2 = entry.getKey();
                        attributeObject(apgcode2, rulestring, symmetry, datastore, writer);
                    }

                } catch (EntityNotFoundException e) {
                    writer.println("The specified tabulation does not appear to exist.");
                }
            }
        }
    }
}
