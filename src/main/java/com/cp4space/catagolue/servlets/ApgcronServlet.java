package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.Soup;
import com.cp4space.catagolue.servlets.CensusServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.apphosting.api.ApiProxy;

public class ApgcronServlet extends HttpServlet {

    public boolean autocron(PrintWriter writer, String irulestring, String isymmetry) {

        if (ApiProxy.getCurrentEnvironment().getRemainingMillis() > 180000) {
            if (irulestring.equals("ob3678s34678")) {
                // Do absolutely nothing.
                writer.println("Ignored due to taboo rulestring.");
            } else {
                Census.fullCronJob(writer, irulestring, isymmetry);
            }
        } else {
            writer.println("Ignored due to insufficient time remaining.");
            return true;
        }

        return false;
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws IOException {
        
        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();
        
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();
        
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (symmetry == null)) {
                rulestring = pathParts[1];
                symmetry = pathParts[2];
            }
        }
        
        if (rulestring == null) {
            writer.println("Executing scheduled task...");

            if (autocron(writer, "b3s23", "C1")) { return; }

            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            Query query = new Query("Census").addSort("uncommittedHauls", Query.SortDirection.DESCENDING);
            PreparedQuery preparedQuery = datastore.prepare(query);
            List<Entity> censuses = preparedQuery.asList(FetchOptions.Builder.withLimit(80));

            for (Entity censusEntity : censuses) {
                String irulestring = (String) censusEntity.getProperty("rulestring");
                String isymmetry = (String) censusEntity.getProperty("symmetries");

                if ((irulestring.equals("b3s23")) && (isymmetry.equals("C1"))) {
                    continue;
                }

                long uchcount = (long) censusEntity.getProperty("uncommittedHauls");

                if (uchcount > 0) {
                    writer.println("*** " + String.valueOf(uchcount)+" uncommitted hauls in "+irulestring+"/"+isymmetry + " ***");
                    if (autocron(writer, irulestring, isymmetry)) { return; }
                }
            }

            if (autocron(writer, "b3s23", "C1")) { return; }

        } else if (symmetry == null) {
            writer.println("Please provide a symmetry type.");
        } else if (CensusServlet.isAdmin()) {
            Census.fullCronJob(writer, rulestring, symmetry);
        } else {
            writer.println("Ignored due to insufficient user privileges.");
        }
    }

}
