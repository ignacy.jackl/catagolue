#!/bin/bash

rulesname="src/main/java/com/cp4space/catagolue/servlets/RulesServlet.java"
linecount="$( cat "$rulesname" | wc -l )" # useful use of cat

grep -B "$linecount" '// START //' "$rulesname" > tmp.java
cat src/main/webapp/autogen/genuslist.py | grep 'genus_list.append' | sed -e 's/^/        addRegex(regices, "/' | sed -e 's/$/");/' >> tmp.java
grep -A "$linecount" '// STOP //' "$rulesname" >> tmp.java

mv tmp.java "$rulesname"

mkdir ~/.ssh/
echo "${CI_KNOWN_HOSTS}" > ~/.ssh/known_hosts
echo "${ED25519_KEY}" > ~/.ssh/id_ed25519
chmod 600 ~/.ssh/id_ed25519

git config user.name "Catagolue update process"
git config user.email "catagolue@hatsya.com"
git add "$rulesname"
git add "src/main/webapp/autogen"
git remote add ssh_origin "git@gitlab.com:apgoucher/catagolue.git"
git commit -m "Update autogenerated files"

status="$?"

if [ "$status" != "0" ]; then
echo "commit failed; assuming no changes were made..."
exit 0
fi

echo "commit succeeded; preparing to push..."
set -e

git push -o ci.skip ssh_origin HEAD:master
