package com.cp4space.catagolue.servlets;

import java.io.PrintWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.patterns.GolObject;
import com.cp4space.payosha256.PayoshaUtils;
import com.cp4space.catagolue.utils.SvgUtils;
import com.cp4space.catagolue.servlets.HashsoupServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;

public class TextSamplesServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        String rulestring = req.getParameter("rule");
        String apgcode = req.getParameter("apgcode");
        String sampstring = "samples";
        String pathinfo = req.getPathInfo();
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (apgcode == null)) {
                apgcode = pathParts[1];
            }
            if ((pathParts.length >= 3) && (rulestring == null)) {
                rulestring = pathParts[2];
            }
            if ((pathParts.length >= 4) && (pathParts[3].length() >= 1)) {
                sampstring = pathParts[3];
            }
        }

        PrintWriter writer = resp.getWriter();

        if (rulestring == null) {
            writer.println("You must specify a rulestring.");
        } else if (apgcode == null || apgcode.length() == 0) {
            writer.println("You must specify an object.");
        } else {
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            String occurrences = Census.getMetadatum(datastore, rulestring, apgcode, sampstring);

            if ((apgcode.length() >= 4) && apgcode.substring(0, 4).equals("xs0_")) {
                String[] parts = occurrences.split("\n");
                for (String part : parts) {
                    if ((part.length() == 0) || (part.charAt(0) == '#') || (part.contains("mark!"))) { continue; }
                    writer.println(part);
                }
            } else {
                writer.println(occurrences);
            }
        }
    }
}
