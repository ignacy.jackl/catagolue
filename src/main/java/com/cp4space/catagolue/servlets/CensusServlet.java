package com.cp4space.catagolue.servlets;

import java.io.PrintWriter;
import java.io.ByteArrayOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import com.cp4space.payosha256.PayoshaUtils;
import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.utils.NumberUtils;
import com.cp4space.catagolue.utils.AlphanumComparator;
import com.cp4space.catagolue.utils.SvgUtils;
import com.cp4space.catagolue.servlets.UserServlet;
import com.cp4space.catagolue.servlets.ObjectServlet;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class CensusServlet extends ExampleServlet {

    public static void requestSymmetry(PrintWriter writer, String rulestring, String urlbase) {
        retrieveContent(writer, rulestring, urlbase, false, 0);
    }

    @Override
    public String getTitle(HttpServletRequest req) { return getRuleTitle(req, "Census"); }

    public static String getRuleTitle(HttpServletRequest req, String defaultTitle) {

        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
        }

        if ((symmetry == null) && (rulestring != null)) {

            Map<String, String> namemap = new HashMap<String, String>();
            CommonNames.getNamemap("rules", namemap, false);

            if (namemap.containsKey(rulestring)) {
                return " (" + rulestring + ")!" + namemap.get(rulestring);
            } else {
                return rulestring;
            }
        } else {
            return defaultTitle;
        }
    }

    public static void generateSymmetry(PrintWriter writer, String rulestring, String urlbase) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        writer.println("<p>Please specify a symmetry type:</p>");           
        Filter propertyFilter = new FilterPredicate("rulestring", FilterOperator.EQUAL, rulestring);
        Query query = new Query("Census").setFilter(propertyFilter).addSort("numobjects", Query.SortDirection.DESCENDING);
        PreparedQuery preparedQuery = datastore.prepare(query);
        Iterable<Entity> censuses = preparedQuery.asIterable(FetchOptions.Builder.withLimit(1000));

        for (int i = 0; i < 4; i++) {

            boolean used = false;
            for (Entity censusEntity : censuses) {

                String keyname = censusEntity.getKey().getName();
                int symstat = ObjectServlet.isUnofficial(keyname);
                if (symstat != i) { continue; }
                long numobjects = (long) censusEntity.getProperty("numobjects");
                if (numobjects > 1) {

                    if (!used) {
                        if (i == 0) { writer.println("<h3>Official symmetries</h3>"); }
                        if (i == 1) { writer.println("<h3>Inflated symmetries</h3>"); }
                        if (i == 2) { writer.println("<h3>Unofficial symmetries</h3>"); }
                        if (i == 3) { writer.println("<h3>Troll symmetries</h3>"); }
                        writer.println("<ul>");
                        used = true;
                    }

                    writer.println("<li>");
                    if (i == 0) { writer.println("<b>"); }
                    writer.println("<a href=\"" + urlbase + keyname+"\">" + keyname + "</a>");
                    writer.println("(" + NumberUtils.illion(numobjects) + " objects)");
                    if (i == 0) { writer.println("</b>"); }
                    writer.println("</li>");
                }
            }
            if (used) { writer.println("</ul>"); }
        }

    }

    @Override
    public void writeHead(PrintWriter writer, HttpServletRequest req) {
        writer.println("<script type=\"text/javascript\" src=\"/js/jquery.min.js\"></script>");
    }

    @Override
    public void writeImage(PrintWriter writer, HttpServletRequest req) {

        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
        }
        if ((symmetry != null) && (symmetry.equals("Saka_Test"))) {
            writer.println("<div id=\"title\"><a href=\"/census/b3s23/Saka_Test\"><img src=\"/images/sakalogue.png\"></a></div>");
        } else {
            super.writeImage(writer, req);
        }

    }

    public static boolean isAdmin() {
        UserService userService = UserServiceFactory.getUserService();
        return (userService.isUserLoggedIn() && userService.isUserAdmin());
    }

    public static String shorten(String keyname, int threshold, int padding) {

        String short_keyname = keyname;
        if (keyname.length() >= threshold) {
            short_keyname = keyname.substring(0, padding) + "..." + keyname.substring(keyname.length()-padding);
        }
        return short_keyname;
    }

    public static String shorten(String keyname) {

        if (ObjectServlet.isUnofficial(keyname) >= 3) { return null; }
        return shorten(keyname, 42, 10);

    }

    public static void retrieveContent(PrintWriter writer, String rulestring, String symmetry, boolean isBackup, long totalObjects) {

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        Key statsKey = KeyFactory.createKey("CensusPage", rulestring + "/" + symmetry);

        try {
            Entity statsEntity = datastore.get(statsKey);
            String content = ((Text) statsEntity.getProperty("data")).getValue();
            Date lastModified = (Date) statsEntity.getProperty("lastModified");

            Date currentTime = new Date();

            long elapsedMillis = currentTime.getTime() - lastModified.getTime();
            long elapsedSeconds = elapsedMillis / 1000;

            if (isBackup || (elapsedSeconds < 600)) {
                writer.println(content);
                if (!isBackup) { writer.println("<p>Last cache update: " + elapsedSeconds + " seconds ago.</p>"); }
                return;
            }

        } catch (EntityNotFoundException e) {
            
        }

        ByteArrayOutputStream bbuffer = new ByteArrayOutputStream();
        PrintWriter writer2 = new PrintWriter(bbuffer);
        generateContent(writer2, rulestring, symmetry, totalObjects);
        writer2.close();

        String content = bbuffer.toString();

        Entity statsEntity = new Entity("CensusPage", rulestring + "/" + symmetry);
        Date date = new Date();
        statsEntity.setProperty("lastModified", date);
        statsEntity.setProperty("data", new Text(content));

        datastore.put(statsEntity);

        writer.println(content);
        writer.println("<p>Last cache update: just now.</p>");
    }

    public static boolean isCensored(String displayedName) {

        String digest = PayoshaUtils.hashString(displayedName);

        if (digest.equals("03db8e472ce8a9dee8fdc028b155d4f1f8d7d4ff9f9ebc702be544d0fc773416")) {
            return true;
        } else if (digest.equals("2266eb7fc368260bac5beecd098be8d0f609f4691a634913471bcc44af29fa61")) {
            return true;
        }

        return false;
    }

    public static void printLeaderboard(PrintWriter writer, Map<String, Long> leadermap, String chartname, String charttitle, String h3) {

        ArrayList<Long> sectorsizes = new ArrayList<Long>();
        ArrayList<ArrayList<String>> sectorlines = new ArrayList<ArrayList<String>>();

        for (int i = 0; i < 3; i++) {
            sectorsizes.add((long) 0);
            sectorlines.add(new ArrayList<String>());
        }

        boolean xmas = ExampleServlet.isChristmas();

        String xolournames[] = {"#c00000", "#b00000", "#a00000", "#900000",
                                "#00c000", "#00b000", "#00a000", "#009000",
                                "#D4AF37", "#CFB53B", "#C5B358", "#ffd700"};
        String colournames[] = {"#4890c0", "#38a8c0", "#18d8c0", "#28c0c0",
                                "#303030", "#503058", "#903098", "#703080",
                                "#c03030", "#c04830", "#c07830", "#c06030"};

        int phase = 2;
        long totobjs = 0;

        for (Map.Entry<String, Long> entry : Census.sortTable(leadermap)) {

            String displayedName = entry.getKey();
            Long quantity = entry.getValue();

            // Only appear on leaderboard if >= one thousandth of a percent:
            if (quantity <= 0) { break; }
            if (quantity < (totobjs / 100000)) { break; }

            if (displayedName == null) { continue; }
            if (isCensored(displayedName)) { continue; }

            totobjs += quantity;

            // Select smallest sector:
            int j = 0;
            for (int i = 1; i < sectorsizes.size(); i++) {
                if (sectorsizes.get(i) < sectorsizes.get(j)) {j = i;}
            }

            int colourint = 4 * ((j + phase) % 3) + 3 - (sectorlines.get(j).size() % 4);
            String colourcode = (xmas ? xolournames[colourint] : colournames[colourint]);

            sectorlines.get(j).add("            {name: '"+displayedName+"', y: "+String.valueOf(quantity)+", color: '"+colourcode+"', url: '/user/"+displayedName.replaceAll(" ", "%20")+"'}");
            sectorsizes.set(j, sectorsizes.get(j) + quantity);
        }

        if (totobjs == 0) { return; }

        writer.println("<h3><a name=\"" + chartname + "\">" + h3 + "</a></h3>");

        writer.println("<p>Click on a sector to display individual user contributions.</p>");

        writer.println("<script type=\"text/javascript\">");
        writer.println("$(function () {");
        writer.println("    $('#" + chartname + "').highcharts({");
        writer.println("        chart: {type: 'pie', backgroundColor: '#C0FFEE', options3d: {enabled: true, alpha: 45, beta: 0}},");
        writer.println("        title: {text: '" + charttitle + "'},");
        writer.println("        tooltip: {pointFormat: '{series.name}: <b>{point.percentage:.3f}%</b>'},");
        writer.println("        plotOptions: {pie: {allowPointSelect: true, minSize: '70%', size: '80%', center: ['50%','50%'], cursor: 'pointer', innerSize: '42%', depth: 57, dataLabels: {enabled: true, format: '{point.name}'}}},");
        writer.println("        series: [{type: 'pie', events: {click: function(e) {location.href = e.point.url; e.preventDefault();}}, name: 'Objects contributed', data: [");


        boolean flag = false;

        for (int i = 0; i < sectorsizes.size(); i++) {
            for (int j = sectorlines.get(i).size() - 1; j >= 0; j--) {
                if (flag) {
                    writer.println(",");
                } else {
                    flag = true;
                }
                writer.print(sectorlines.get(i).get(j));
            }
        }

        if (flag) { writer.println(); }

        writer.println("        ]}]");
        writer.println("    });");
        writer.println("});");
        writer.println("</script>");
        writer.println("<div id=\"" + chartname + "\" style=\"height: 400px\"></div>");
    }

    public static void generateContent(PrintWriter writer, String rulestring, String symmetry, long totalObjects) {

        if (symmetry.charAt(symmetry.length() - 1) == '/') {
            if (RulesServlet.beginNavigation(writer, rulestring)) {
                RulesServlet.endNavigation(writer);
            }
            generateSymmetry(writer, rulestring, symmetry);
            return;
        }

        if (RulesServlet.beginNavigation(writer, rulestring)) {
            RulesServlet.addNavigationLink(writer, "/census/" + rulestring + "/" + symmetry, symmetry);
            RulesServlet.endNavigation(writer);
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        try {
            Key leaderboardKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Leaderboard", "numobjects").getKey();
            Entity leaderboardEntity = datastore.get(leaderboardKey);
            writer.println("<script src=\"https://code.highcharts.com/highcharts.js\"></script>");
            writer.println("<script src=\"https://code.highcharts.com/highcharts-3d.js\"></script>");
            writer.println("<script src=\"https://code.highcharts.com/modules/exporting.js\"></script>");

            String[] lines = ((Text) leaderboardEntity.getProperty("data")).getValue().split("\n");

            boolean isLife = rulestring.equals("b3s23");

            Map<String, Long> leadermap = new HashMap<String, Long>();

            for (String line : lines) {
                if (line != null && line.length() >= 1) {
                    String[] parts = line.split(" ", 2);
                    Long quantity = Long.valueOf(parts[0]);

                    // Escape single quotes:
                    String displayedName = parts[1].replaceAll("'","&#39;").replaceAll("\\\\","\\\\\\\\");

                    if ((!isLife) && (quantity >= 333333333333L)) {
                        UserServlet.createBadge(datastore, displayedName, "Sprotsmanship");
                    }

                    leadermap.put(displayedName, quantity);
                }
            }

            printLeaderboard(writer, leadermap, "piechart", "Objects per contributor since 2015-02-27", "All-time contribution leaderboard");

            String targetDate = BackupServlet.getTodaysDate().substring(0, 8) + "01";

            leaderboardKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry + "-" + targetDate).addChild("Leaderboard", "numobjects").getKey();
            leaderboardEntity = datastore.get(leaderboardKey);
            lines = ((Text) leaderboardEntity.getProperty("data")).getValue().split("\n");
            
            for (String line : lines) {
                if (line != null && line.length() >= 1) {
                    String[] parts = line.split(" ", 2);
                    Long quantity = Long.valueOf(parts[0]);
                    String displayedName = parts[1].replaceAll("'","&#39;").replaceAll("\\\\","\\\\\\\\");
                    leadermap.put(displayedName, leadermap.get(displayedName) - quantity);
                }
            }

            printLeaderboard(writer, leadermap, "piechart2", "Objects per contributor since " + targetDate, "Monthly contribution leaderboard");

        } catch (EntityNotFoundException e) {

        }

        Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);
        Query query = new Query("Tabulation", censusKey).setKeysOnly();
        PreparedQuery preparedQuery = datastore.prepare(query);
        List<Entity> tabulations = preparedQuery.asList(FetchOptions.Builder.withLimit(10000));

        Map<String, Set<String>> preprefices = new TreeMap<String, Set<String>>();

        for (Entity tabulation : tabulations) {
            String prefix = tabulation.getKey().getName();
            if (prefix.equals("meth") || prefix.equals("z") || prefix.equals("xs0")) { continue; }
            String preprefix = (prefix.length() > 2) ? prefix.substring(0, 2) : prefix;

            if (!preprefices.containsKey(preprefix)) {
                preprefices.put(preprefix, new TreeSet<String>(new AlphanumComparator()));
            }

            preprefices.get(preprefix).add(prefix);
        }

        writer.println("<h3><a name=\"tabulations\">Object tabulations</a></h3>");
        writer.println("<table><tr>");

        for (int i = 0; i < 3; i++) {

            for (String preprefix : preprefices.keySet()) {

                Set<String> prefices = preprefices.get(preprefix);

                int j = ("xyz".contains(preprefix.substring(0, 1))) ? 0 : 1;
                j += (preprefix.equals("xs") ? 0 : 1);
                if (i != j) { continue; }

                writer.println("<td valign=\"top\"><h4>");
                if (preprefix.equals("ov")) {writer.println("Oversized"); }
                if (preprefix.equals("zz")) {writer.println("Unusual growth"); }
                if (preprefix.equals("xs")) {writer.println("Still lifes"); }
                if (preprefix.equals("xp")) {writer.println("Oscillators"); }
                if (preprefix.equals("xq")) {writer.println("Spaceships"); }
                if (preprefix.equals("yl")) {writer.println("Linear growth"); }
                if (preprefix.equals("me")) {writer.println("Long-lived patterns"); }
                writer.println("</h4><ul>");

                for (String prefix : prefices) {
                    String p2 = prefix.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
                    writer.println("<li><a href=\"/census/"+rulestring+"/"+symmetry+"/"+p2+"\">"+p2+"</a></li>");
                }

                writer.println("</ul></td>");
            }

        }

        writer.println("</tr></table>");

    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        String tabprefix = req.getParameter("tabulation");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String thingmode = req.getParameter("mode");
        String pathinfo = req.getPathInfo();

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (tabprefix == null)) {
                tabprefix = pathParts[3];
            }
            if ((pathParts.length >= 5) && (thingmode == null)) {
                thingmode = pathParts[4];
            }
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        if ((rulestring == null) || (rulestring.equals(""))) {

            writer.println("<p>You must specify a rulestring (e.g. <a href=\"/census/b3s23\">b3s23</a>) and symmetry type.</p>");

            writer.println("<table width=\"100%\"><tr><th style=\"text-align:left\">Most popular</th>");
            writer.println("<th style=\"text-align:right\">Recently updated</th></tr>");

            Query query = new Query("Census").addSort("numobjects", Query.SortDirection.DESCENDING);
            PreparedQuery preparedQuery = datastore.prepare(query);
            List<Entity> censuses = preparedQuery.asList(FetchOptions.Builder.withLimit(80));

            writer.println("<tr><td style=\"text-align:left\"><ul style=\"list-style: none;\">");

            int cinl = 0;
            boolean found_main = false;

            for (Entity censusEntity : censuses) {
                String keyname = censusEntity.getKey().getName();
                String short_keyname = shorten(keyname);
                if (short_keyname == null) { continue; }
                if (keyname.equals("b3s23/C1")) { found_main = true; }
                long numobjects = (long) censusEntity.getProperty("numobjects");
                long oneMillion = 1000000;
                long oneTrillion = oneMillion * oneMillion;

                boolean bigCensus = (numobjects >= oneTrillion);

                if ((!found_main) || (cinl >= 70)) { continue; }

                cinl += 1;

                if (ObjectServlet.isUnofficial(keyname.split("/")[1]) >= 2) {
                    bigCensus = false;
                } else if (!(keyname.split("/")[0].matches("b[1-8]*s[0-8]*"))) {
                    bigCensus = false;
                }

                if (numobjects > 0) {
                    writer.println("<li><a href=\"/census/"+keyname+"\">"+short_keyname+"</a>");
                    if (bigCensus) { writer.println("<b>"); }
                    if (short_keyname.length() <= 25) {
                        writer.println("(" + NumberUtils.illion(numobjects) + " objects)");
                    }
                    if (bigCensus) { writer.println("*</b>"); }
                    writer.println("</li>");
                }
            }

            writer.println("</ul></td>");

            query = new Query("Census").addSort("lastModified", Query.SortDirection.DESCENDING);
            preparedQuery = datastore.prepare(query);
            censuses = preparedQuery.asList(FetchOptions.Builder.withLimit(80));

            writer.println("<td style=\"text-align:right\"><ul style=\"list-style: none;\">");

            cinl = 0;

            for (Entity censusEntity : censuses) {
                String keyname = censusEntity.getKey().getName();
                String short_keyname = shorten(keyname);
                if (short_keyname == null) { continue; }
                if (cinl >= 70) { continue; }
                cinl += 1;
                Date lastModified = (Date) censusEntity.getProperty("lastModified");
                writer.println("<li><a href=\"/census/"+keyname+"\">"+short_keyname+"</a>");
                if (short_keyname.length() <= 25) {
                    writer.println("(" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(lastModified) + ")");
                }
                writer.println("</li>");
            }

            writer.println("</ul></td></tr></table>");

            writer.println("<p>*censuses of isotropic 2-state Moore-neighbourhood rules "
                            + "in official symmetries with at least one trillion objects "
                            + "are protected by a combination of statistical tests, "
                            + "peer-verification, and object validity checks.</p>");

            writer.println("<p>Note: timestamps are in UTC and object "
                            + "counts use the short scale (billion = 10<sup>9</sup>; "
                            + "trillion = 10<sup>12</sup>).</p>");

        } else if ((symmetry == null) || (symmetry.equals(""))) {

            requestSymmetry(writer, rulestring, "/census/");

        } else if ((tabprefix == null) || (tabprefix.equals(""))) {

            Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

            try {
                Entity censusEntity = datastore.get(censusKey);

                long committedHauls = (long) censusEntity.getProperty("committedHauls");
                long uncommittedHauls = (long) censusEntity.getProperty("uncommittedHauls");
                long numsoups = (long) censusEntity.getProperty("numsoups");
                long numobjects = (long) censusEntity.getProperty("numobjects");

                if (numsoups < 0) { numsoups = 0 - numsoups; }
                if (numobjects < 0) { numobjects = 0 - numobjects; }

                String[] symParts = symmetry.split("-", 2);

                boolean isBackup = (symParts.length == 2) && (symParts[1].charAt(0) == '2'); // stops working in 3000 AD

                if (isBackup) {
                    writer.println("<p><b>This is a backup of the census generated on " + symParts[1] + ".");
                    writer.println("For the latest version of the census, see");
                    writer.println("<a href=\"/census/" + rulestring + "/" + symParts[0] + "\">here</a>.</b></p>");
                } else if (uncommittedHauls > 0) {
                    writer.println("<p><b>There are still <a href=\"/haul/" + rulestring + "/" + symmetry +
                            "?committed=0\"> uncommitted hauls</a> uploaded by "
                            + "apgsearch but not yet included in the census.");
                    if (isAdmin()) {
                        writer.println("Click <a href=\"/apgcron/" + rulestring + "/" + symmetry +
                                "\">here</a> to commit them to the census.");
                    }
                    writer.println("</b></p>");
                }

                if (symmetry.equals("asymmetric-soups")) {
                    writer.println("<p align=\"center\">The following census was");
                    writer.println("generated from "+NumberUtils.commaChameleon(numsoups)+" soups.</p>");
                }

                if (committedHauls > 0) {
                    writer.println("<p align=\"center\">The following census was compiled from");
                    if (!isBackup) {
                        writer.print("<a href=\"/haul/" + rulestring + "/" + symmetry + "?committed=");
                        writer.println(String.valueOf(Census.JUST_COMMITTED) + "\">");
                        writer.println(NumberUtils.commaChameleon(committedHauls) + " committed hauls&hellip;</a>");
                    } else {
                        writer.println(NumberUtils.commaChameleon(committedHauls) + " committed hauls&hellip;");
                    }
                    writer.println("<br /> &hellip;containing "+NumberUtils.commaChameleon(numobjects)+" objects&hellip;");
                    writer.println("<br /> &hellip;generated from "+NumberUtils.commaChameleon(numsoups)+" soups.</p>");
                }

                /*
                 * It's okay for this to be cached, because the individual
                 * tabulations themselves are up-to-date:
                 */
                retrieveContent(writer, rulestring, symmetry, isBackup, numobjects);

            } catch (EntityNotFoundException e) {
                writer.println("<p>It appears that no-one has yet investigated this combination of rule "
                        + "and symmetry options. If you're interested, investigate the rule yourself using "
                        + "<a href=\"/apgsearch\">apgsearch</a>.</p>");
            }

        } else if (tabprefix.equals("CENSUS")) {

            Key censusKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).getKey();
            if (isAdmin()) {
                if (thingmode != null) {
                    if (thingmode.equals("DELETE")) {
                        datastore.delete(censusKey);
                    }
                    if (thingmode.equals("RESYNC")) {

                        Query query = new Query("Tabulation", censusKey);
                        PreparedQuery preparedQuery = datastore.prepare(query);
                        Iterable<Entity> tabulations = preparedQuery.asIterable(FetchOptions.Builder.withLimit(1000));

                        long actual_numobjects = 0;

                        for (Entity tabulation : tabulations) {

                            String prefix = tabulation.getKey().getName();
                            String preprefix = prefix.substring(0, 2);

                            if (preprefix.equals("me")) { continue; }

                            Census census = new Census();
                            census.affineAdd(Tabulation.getCensusData(tabulation), 1);
                            actual_numobjects += census.totobjs();

                        }

                        try {
                            Entity censusEntity = datastore.get(censusKey);

                            long numsoups = (long) censusEntity.getProperty("numsoups");
                            long numobjects = (long) censusEntity.getProperty("numobjects");

                            double ratio = ((double) numobjects) / ((double) numsoups);

                            long actual_numsoups = (long) (((double) actual_numobjects) / ratio);
                            censusEntity.setProperty("numsoups", actual_numsoups);
                            censusEntity.setProperty("numobjects", actual_numobjects);
                            datastore.put(censusEntity);

                        } catch (EntityNotFoundException e) {
                            // This will never happen.
                        }
                    }
                }
            }
        } else {

            if (RulesServlet.beginNavigation(writer, rulestring)) {
                RulesServlet.addNavigationLink(writer, "/census/" + rulestring + "/" + symmetry, symmetry);
                RulesServlet.addNavigationLink(writer, "/census/" + rulestring + "/" + symmetry + "/" + tabprefix, tabprefix);
                RulesServlet.endNavigation(writer);
            }

            try {

                Map<String, String> namemap = new HashMap<String, String>();
                CommonNames.getNamemap("std", namemap, true);

                Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Tabulation", tabprefix).getKey();
                Entity tabulation = datastore.get(tabulationKey);

                Date tlm = (Date) tabulation.getProperty("lastModified");

                if (isAdmin()) {
                    if (thingmode != null) {
                        if (thingmode.equals("DELETE")) {
                            datastore.delete(tabulationKey);
                        }
                    }
                }

                String offsetString = req.getParameter("offset");
                int offsetInt = (offsetString == null) ? 0 : Integer.valueOf(offsetString);
                int displayedInt = 200;

                if (tabprefix.length() >= 2 && tabprefix.charAt(0) == 'x') {
                    if (tabprefix.charAt(1) == 's') {
                        displayedInt = 200;
                    } else if (tabprefix.charAt(1) == 'q') {
                        displayedInt = 10;
                    } else {
                        if ((tabprefix.length() == 3) || (tabprefix.length() >= 5)) {
                            displayedInt = 50;
                        } else if ((tabprefix.charAt(1) == '1') || (tabprefix.charAt(1) == '2')) {
                            displayedInt = 20;
                        } else {
                            displayedInt = 10;
                        }
                    }
                }

                BufferedReader bufReader = Tabulation.getCensusStream(tabulation);
                List<String> relevantRows = new ArrayList<>();

                String line = null;
                int i = 0;

                try {
                    while ((line = bufReader.readLine()) != null) {
                        i++;
                        if ((i > offsetInt) && (i <= offsetInt + displayedInt)) {
                            relevantRows.add(line);
                        }
                    }
                } catch(IOException e) {
                    // This will never happen in practice
                }

                int hbytes = HaulServlet.estimateEntitySize(tabulation);

                writer.println("<p>This tabulation occupies an estimated " + String.valueOf(hbytes >> 10) +
                        " kilobytes of memory, out of a maximum of 1000. The entire tabulation may be downloaded in" +
                        " <a href=\"/textcensus/" + rulestring + "/" + symmetry + "/" + tabprefix + "\">CSV format</a>.<p>");

                if (relevantRows.size() == 0) {
                    writer.println("<p>There are no further objects to be displayed.</p>");
                    return;
                }

                // Lazy way to print the bar both above and below the table:
                for (int rubbish = 0; rubbish < 2; rubbish++) {

                    int lowerbound = offsetInt + 1;
                    int upperbound = offsetInt + relevantRows.size();

                    writer.println("<p>Displaying " + lowerbound + " to " + upperbound + " of <b>" + i + "</b> objects");

                    if (offsetInt > 0) {
                        int prevOffset = Math.max(0, offsetInt - displayedInt);
                        writer.println("(");
                        writer.println("<a href=\"/census/"+rulestring+"/"+symmetry+"/"+tabprefix+"?offset="+String.valueOf(prevOffset)+"\">");
                        writer.println("prev " + String.valueOf(displayedInt)+"</a>");
                        if (offsetInt + displayedInt < i) {
                            writer.println("|");
                        } else {
                            writer.println(")");
                        }
                    }

                    if (offsetInt + displayedInt < i) {
                        if (offsetInt <= 0) {
                            writer.println("(");
                        }

                        int nextOffset = offsetInt + displayedInt;

                        writer.println("<a href=\"/census/"+rulestring+"/"+symmetry+"/"+tabprefix+"?offset="+String.valueOf(nextOffset)+"\">");
                        writer.println("next " + String.valueOf(Math.min(displayedInt, i - nextOffset))+"</a>");

                        writer.println(")");
                    }

                    writer.println("</p>");

                    if (rubbish == 1) { break; }

                    String countHeader = (symmetry.equals("synthesis-costs") ? ((tabprefix.charAt(0) == 'g') ? "Bounding box" : "Glider cost") : "Occurrences");

                    writer.println("<table cellspacing=1 border=2 cols=2>");
                    writer.println("<tr><th>Rank</th><th>Image</th><th>Object</th><th>" + countHeader + "</th></tr>");

                    int ii = offsetInt;

                    for (String row : relevantRows) {
                        ii++;
                        if ((ii >= lowerbound) && (ii <= upperbound)) {
                            String apgcode = row.split(" ")[0];
                            String count = NumberUtils.commaChameleon(Long.valueOf(row.split(" ")[1]));
                            if (namemap.containsKey(apgcode)) {
                                apgcode += " (" + namemap.get(apgcode) + ")";
                            }
                            SvgUtils.apgRow(ii, writer, rulestring, apgcode, count);
                        }
                    }

                    writer.println("</table>");

                }

                if (tlm != null) {
                    writer.println("<p>Last modified at " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(tlm) + "</p>");
                }

            } catch (EntityNotFoundException e) {
                writer.println("<p>The specified tabulation does not appear to exist.</p>");
            }
        }
    }
}
