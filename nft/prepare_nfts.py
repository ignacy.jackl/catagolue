
import lifelib
import imageio
import os
import sys
import json

from datetime import datetime as dt
from math import gcd
from lifelib.pythlib.pattern import determine_if_gun

def prepare_nft(d, lt, output_dir):

    json_file = os.path.join(output_dir, ('%d.json' % d['id']))
    gif_file = os.path.join(output_dir, ('%d.gif' % d['id']))

    if os.path.exists(gif_file) and os.path.exists(json_file):
        return

    timestamp = int(dt.strptime(d['discoverydate'], "%Y-%m-%d").timestamp()) + 43200

    # check apgcode is canonical:
    apgcode = d['apgcode']
    pat = lt.pattern(apgcode)
    canonical_apgcode = pat.apgcode
    if (canonical_apgcode != apgcode):
        raise ValueError("Non-canonical apgcode error for NFT %d (%s != %s)" % (d['id'], apgcode, canonical_apgcode))

    # generate animated GIF:
    print('Generating animated GIF for NFT %d (apgcode %s)' % (d['id'], apgcode))
    pat.make_gif(filename=gif_file, hue=d['hue'], width=350, height=350)

    # create metadata:
    print('Generating metadata for NFT %d (apgcode %s)' % (d['id'], apgcode))

    catagolue_url = ('https://catagolue.hatsya.com/nfts/%d' % d['id'])

    external_pages = []

    if '/forums/' in d['wikipage']:
        external_pages.append('[forum post](%s)' % d['wikipage'])
    elif '/wiki/' in d['wikipage']:
        external_pages.append('[LifeWiki page](%s)' % d['wikipage'])
    else:
        external_pages.append('[external page](%s)' % d['wikipage'])

    external_pages.append('[Catagolue page](%s)' % catagolue_url)

    external_pages = ' and '.join(external_pages)

    m = {'image': ('https://catagolue.hatsya.com/autogen/nfts/%d.gif' % d['id']),
        'external_url': catagolue_url,
        'name': d['name'],
        'attributes': [{'display_type': 'date', 'trait_type': 'discovery date', 'value': timestamp}],
        'background_color': '000000',
        'description': d['description'] + ('''

Discovered by %s on %s.

See the %s for further information.''' % (d['discoverer'], d['discoverydate'], external_pages))}

    m['attributes'].append({'trait_type': 'object type',
                            'value':  { 'xs': 'still life',
                                        'xp': 'oscillator',
                                        'xq': 'spaceship',
                                        'xg': 'gun'}[apgcode[:2]]})

    period = 1 if apgcode.startswith('xs') else int(apgcode.split('_')[0][2:])

    m['attributes'].append({'trait_type': 'period', 'value': period})

    if apgcode.startswith('xq'):

        disp = pat.displacement
        disp = sorted([abs(x) for x in disp])[::-1]

        vel_gcd = gcd(period, gcd(*disp))
        vel_denom = period // vel_gcd
        vel_num = tuple([x // vel_gcd for x in disp])

        if vel_num[1] == 0:
            direction = 'orthogonal'
            speed = '%dc/%d orthogonal' % (vel_num[0], vel_denom)
        elif vel_num[1] == vel_num[0]:
            direction = 'diagonal'
            speed = '%dc/%d diagonal' % (vel_num[0], vel_denom)
        else:
            direction = 'oblique'
            speed = ('%sc/%d' % (vel_num, vel_denom)).replace(' ', '')

        if speed.startswith('1c'):
            speed = speed[1:]

        m['attributes'].append({'trait_type': 'direction',
                                'value': direction})
        m['attributes'].append({'trait_type': 'speed',
                                'value': speed})

    else:

        if apgcode.startswith('xg'):
            bg = determine_if_gun(pat, 16)[4]
        else:
            bg = lt.pattern('', 'LifeHistory')
            bg += pat
            bg = bg[period]

        rect = bg.getrect()

        m['attributes'].append({'trait_type': 'bounding area', 'value': rect[2] * rect[3]})

    with open(json_file, 'w') as f:
        json.dump(m, f, indent=4)


def main(input_file, output_dir):

    with open(input_file) as f:
        x = json.load(f)

    x = x['data']
    if len(x) == 0:
        raise ValueError("No NFTs found in JSON file.")

    print("%d NFTs found." % len(x))

    lt = lifelib.load_rules('b3s23').lifetree(n_layers=4)

    for d in x:
        prepare_nft(d, lt, output_dir)

    from web3.auto.infura import w3

    abi = '[ { "inputs": [], "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "owner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "approved", "type": "address" }, { "indexed": true, "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "Approval", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "owner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "operator", "type": "address" }, { "indexed": false, "internalType": "bool", "name": "approved", "type": "bool" } ], "name": "ApprovalForAll", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "previousOwner", "type": "address" }, { "indexed": true, "internalType": "address", "name": "newOwner", "type": "address" } ], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": true, "internalType": "address", "name": "from", "type": "address" }, { "indexed": true, "internalType": "address", "name": "to", "type": "address" }, { "indexed": true, "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "Transfer", "type": "event" }, { "inputs": [ { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "approve", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "owner", "type": "address" } ], "name": "balanceOf", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "getApproved", "outputs": [ { "internalType": "address", "name": "", "type": "address" } ], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "getNextTokenId", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "owner", "type": "address" }, { "internalType": "address", "name": "operator", "type": "address" } ], "name": "isApprovedForAll", "outputs": [ { "internalType": "bool", "name": "", "type": "bool" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "_to", "type": "address" } ], "name": "mintTo", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "name": "name", "outputs": [ { "internalType": "string", "name": "", "type": "string" } ], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "owner", "outputs": [ { "internalType": "address", "name": "", "type": "address" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "ownerOf", "outputs": [ { "internalType": "address", "name": "", "type": "address" } ], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "renounceOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "from", "type": "address" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "safeTransferFrom", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "from", "type": "address" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "tokenId", "type": "uint256" }, { "internalType": "bytes", "name": "_data", "type": "bytes" } ], "name": "safeTransferFrom", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "operator", "type": "address" }, { "internalType": "bool", "name": "approved", "type": "bool" } ], "name": "setApprovalForAll", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "bytes4", "name": "interfaceId", "type": "bytes4" } ], "name": "supportsInterface", "outputs": [ { "internalType": "bool", "name": "", "type": "bool" } ], "stateMutability": "view", "type": "function" }, { "inputs": [], "name": "symbol", "outputs": [ { "internalType": "string", "name": "", "type": "string" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "uint256", "name": "index", "type": "uint256" } ], "name": "tokenByIndex", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "owner", "type": "address" }, { "internalType": "uint256", "name": "index", "type": "uint256" } ], "name": "tokenOfOwnerByIndex", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "uint256", "name": "_tokenId", "type": "uint256" } ], "name": "tokenURI", "outputs": [ { "internalType": "string", "name": "", "type": "string" } ], "stateMutability": "pure", "type": "function" }, { "inputs": [], "name": "totalSupply", "outputs": [ { "internalType": "uint256", "name": "", "type": "uint256" } ], "stateMutability": "view", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "from", "type": "address" }, { "internalType": "address", "name": "to", "type": "address" }, { "internalType": "uint256", "name": "tokenId", "type": "uint256" } ], "name": "transferFrom", "outputs": [], "stateMutability": "nonpayable", "type": "function" }, { "inputs": [ { "internalType": "address", "name": "newOwner", "type": "address" } ], "name": "transferOwnership", "outputs": [], "stateMutability": "nonpayable", "type": "function" } ]'

    c = w3.eth.contract(address='0xE4Fa7749611334fc40f27F188A2f5e5b7016819B', abi=abi)
    mintedTokens = c.functions.getNextTokenId().call()

    mapping = []
    mapping2 = []
    for d in x:
        tokenId = d['id']
        state = 'minted' if (tokenId < mintedTokens) else 'unminted'
        mapping.append('    "%s" : "%d:%s",\n' % (d['apgcode'], tokenId, state))

        if state == 'minted':
            # determine who owns it:
            state = c.functions.ownerOf(tokenId).call()

        mapping2.append('    "%d" : "%s|%s|%s|%s|%s|%s|%s",\n' % (tokenId, d['name'], state, d['apgcode'], d['discoverer'], d['discoverydate'], d['wikipage'], d['recipient']))

    with open('initialise/nft-mapping.txt', 'w') as f:
        f.write("mapping = {\n")
        for l in mapping:
            f.write(l)
        f.write("}\n")

    with open('initialise/reverse-nft-mapping.txt', 'w') as f:
        f.write("mapping = {\n")
        for l in mapping2:
            f.write(l)
        f.write("}\n")


if __name__ == '__main__':

    if len(sys.argv) != 3:
        raise ValueError("Usage: python3 prepare_nfts.py all_nfts.txt output_dir")

    main(sys.argv[1], sys.argv[2])
