package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.payosha256.PayoshaUtils;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class PostPayoshaKeyServlet extends HttpServlet {

    public void keyError(HttpServletResponse resp, String errorMessage) throws IOException {
        PrintWriter writer = resp.getWriter();
        writer.println("Error: " + errorMessage);
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String payoshaKey = req.getParameter("payoshaKey");
        String displayedName = req.getParameter("displayedName");

        displayedName = displayedName.replace("<", "").replace("\"", "").replace(">", "").replace("&", " and ");
        displayedName = displayedName.trim().replaceAll(" +", " ");

        UserService userService = UserServiceFactory.getUserService();

        if (!(userService.isUserLoggedIn() && userService.isUserAdmin())) {
            // no impersonating me:
            displayedName = displayedName.replace("goucher", "g0ucher");
        }

        if (payoshaKey == null || payoshaKey.length() == 0) {
            keyError(resp, "Payosha256 key must not be empty.");
        } else if (payoshaKey.indexOf(':') >= 0) {
            keyError(resp, "Payosha256 key must not contain a : or #.");
        } else if (payoshaKey.indexOf('#') < 0 || (userService.isUserLoggedIn() && userService.isUserAdmin())) {

            boolean alreadyExists = PayoshaUtils.postKey("catagolue", payoshaKey, displayedName);

            if (alreadyExists) {
                keyError(resp, "Payosha256 key already exists.");
            } else {
                resp.sendRedirect(req.getParameter("redirectLocation"));
            }
        } else {
            keyError(resp, "Payosha256 key must not contain a : or #.");
        }
    }
}
