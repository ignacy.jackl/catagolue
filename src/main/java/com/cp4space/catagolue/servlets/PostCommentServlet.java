package com.cp4space.catagolue.servlets;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PostCommentServlet extends HttpServlet {

    public static void rickroll(HttpServletResponse resp) throws IOException {

        String centre = (Math.random() < 0.9) ? "4w9Wg" : "TiLy1";
        resp.sendRedirect("https://www.youtube.com/watch?v=dQw" + centre + "XcQ");

    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws IOException {
        UserService userService = UserServiceFactory.getUserService();
        User user = userService.getCurrentUser();
        boolean is_admin = (userService.isUserLoggedIn() && userService.isUserAdmin());

        String threadName = req.getParameter("threadName");
        Key threadKey = KeyFactory.createKey("CommentThread", threadName);
        String content = req.getParameter("content");

        if (content == null || (!(userService.isUserLoggedIn())) || content.contains("href") || content.length() <= 1 || content.contains(".js")) {

            // Rickroll the potential spammer:
            rickroll(resp);

        } else if (content.length() >= 5 && (content.substring(content.length() - 5).equals(".html"))) {

            // Another common form of spam:
            rickroll(resp);

        } else {

            Date date = new Date();
            Entity comment = new Entity("Comment", threadKey);
            comment.setProperty("user", user);
            comment.setProperty("admin", is_admin);
            comment.setProperty("date", date);
            comment.setProperty("content", new Text(content));

            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            datastore.put(comment);

            resp.sendRedirect(req.getParameter("redirectLocation"));
        }
    }
}
