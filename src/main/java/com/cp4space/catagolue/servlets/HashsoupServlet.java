package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Soup;

public class HashsoupServlet extends HttpServlet {

    public static String recapitalise(String rulestring) {

        Set<Integer> il = new TreeSet<>();

        int i = 0;
        while (i < rulestring.length() - 1) {
            if (rulestring.charAt(i) == 'x') {
                int j = 0; i += 1;
                while ((rulestring.charAt(i) >= '0') && (rulestring.charAt(i) <= '9')) {
                    j *= 10;
                    j += (rulestring.charAt(i) - '0');
                    i += 1;
                }
                il.add(j);
            } else {
                break;
            }
        }

        String newrule = "";
        int j = 0;
        while (i < rulestring.length()) {
            if (il.contains(j)) {
                newrule = newrule + rulestring.substring(i, i+1).toUpperCase();
            } else {
                newrule = newrule + rulestring.substring(i, i+1);
            }
            j += 1;
            i += 1;
        }

        return newrule;
    }

    public static String gollifyRule(String rulestring) {

        if (rulestring.charAt(0) == 'x') {
            return recapitalise(rulestring);
        }

        String slashed = rulestring.toLowerCase().replaceAll("s", "/S").replaceFirst("b", "B");

        boolean hexagonal = slashed.charAt(slashed.length() - 1) == 'h';
        if (hexagonal) { slashed = slashed.substring(0, slashed.length() - 1); }

        String colours = "0";

        if (slashed.charAt(0) == 'g') {
            int i = 0;
            for (i = 1; i < slashed.length(); i++) {
                char c = slashed.charAt(i);
                if (('0' > c) || (c > '9')) { break; }
            }
            if (i <= 1) { return rulestring; }
            colours = slashed.substring(1, i);
            slashed = slashed.substring(i);
        }

        if (slashed.charAt(0) == 'r') {
            if (!(slashed.contains("t"))) {
                // LifeViewer uses lifelib convention for HROT rulestrings:
                return rulestring;
            }
            String[] slashparts = slashed.substring(1).replace("t", "..").replace("B", "/").split("/");
            if (slashparts.length != 3) { return rulestring; }
            String range = slashparts[0];
            slashed = "R" + range + ",C" + colours + ",M1," + slashparts[2] + ",B" + slashparts[1] + ",NM";
        } else if (!colours.equals("0")) {
            String[] slashparts = slashed.replaceAll("BS", "").split("/");
            if (slashparts.length != 2) { return rulestring; }
            slashed = slashparts[1] + "/" + slashparts[0] + "/" + colours;
        }

        if (slashed.contains("f")) {
            // BSFKL
            slashed = slashed.replace("f", "/F");
            slashed = slashed.replace("k", "/K");
            slashed = slashed.replace("l", "/L");
        }

        if (slashed.contains("/")) {
            // genext
            slashed = slashed.replace("d", "/D");
        }

        if (hexagonal) { slashed = slashed + "H"; }

        return slashed;

    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();

        String topology = null;
        String prehash = req.getParameter("prehash");
        String symmetry = req.getParameter("symmetry");
        String rulestring = req.getParameter("rulestring");
        String pathinfo = req.getPathInfo();
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 3) && (prehash == null)) {
                symmetry = pathParts[1];
                prehash = pathParts[2];
            }
            if ((pathParts.length >= 4) && (rulestring == null)) {
                rulestring = pathParts[3];
            }
        }

        String slashed = (rulestring == null) ? "B3/S23" : gollifyRule(rulestring);
        if (symmetry == null) { symmetry = "C1"; }

        String[] symParts = symmetry.split("~");
        if (symParts.length == 2) { symmetry = symParts[1]; topology = symParts[0]; }

        if (prehash == null) {
            writer.println("Please provide a string to hash and an optional symmetry argument.");
        } else if (symmetry.contains("stdin")) {
            String[] hashParts = prehash.split("-");
            boolean printing = false;
            writer.println("x = 0, y = 0, rule = " + slashed);
            for (String hpart : hashParts) {
                if (printing) { writer.println(hpart); }
                printing = true;
            }
        } else if (symmetry.equals("SS")) {
            String[] hashParts = prehash.split("_");
            String[] seedParts = hashParts[hashParts.length - 1].split("!");
            if (seedParts.length != 2) {
                writer.println("Invalid seed.");
            } else {
                String w = "#C";
                String t = seedParts[1];
                int lanenum = 0;
                for (int i = 0; i < t.length(); i++) {
                    char c = t.charAt(i);
                    if ((c >= 'a') && (c <= 'p')) {
                        lanenum = (lanenum << 4) + (c - 'a');
                    } else {
                        int x = 0;
                        if ((c >= '0') && (c <= '9')) { x = c - '0'; }
                        if ((c >= 'A') && (c <= 'V')) { x = 10 + c - 'A'; }
                        lanenum = (lanenum << 3) + (x >> 2);
                        lanenum = (lanenum << 1) + 1;
                        if ((x & 2) != 0) { lanenum = -lanenum; }
                        w += (" " + String.valueOf(lanenum) + (((x & 1) != 0) ? "O" : "E"));
                        lanenum = 0;
                    }
                }

                int dollars = 9;
                String s = seedParts[0];
                writer.println(w);
                String[] gliders = w.split(" ");
                String ll = String.valueOf(gliders.length * 128);
                writer.println("x = " + ll + ", y = " + ll + ", rule = " + slashed);
                writer.println(s + String.valueOf(dollars) + "$");
                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == '$') { dollars += 1; } 
                }
                for (int i = 1; i < gliders.length; i++) {
                    String g = gliders[i];
                    String pglid = "";
                    lanenum = (Integer.valueOf(g.substring(0, g.length() - 1)) - 1) / 2;
                    if (g.charAt(g.length() - 1) == 'E') {
                        pglid += String.valueOf(lanenum + dollars) + "b3o$";
                        pglid += String.valueOf(lanenum + dollars) + "bo$";
                        pglid += String.valueOf(lanenum + dollars + 1) + "bo126$";
                        dollars += 128;
                    } else if (g.charAt(g.length() - 1) == 'O') {
                        pglid += String.valueOf(lanenum + dollars + 1) + "b2o$";
                        pglid += String.valueOf(lanenum + dollars) + "b2o$";
                        pglid += String.valueOf(lanenum + dollars + 2) + "bo126$";
                        dollars += 128;
                    }
                    if (i + 1 == gliders.length) { pglid += "!"; }
                    writer.println(pglid);
                }
            }
        } else {
            if (symmetry.charAt(0) == 'G') {
                symmetry = "C" + symmetry.substring(1);
            } else if (symmetry.charAt(0) == 'H') {
                symmetry = "D" + symmetry.substring(1);
            }

            byte[][] soupArray = Soup.hashSoup(prehash, symmetry);

            int minx = 31;
            int miny = 31;

            for (int y = 0; y < 32; y++) {
                for (int x = 0; x < 32; x++) {
                    if (soupArray[x][y] > 0) {
                        minx = Math.min(x, minx);
                        miny = Math.min(y, miny);
                    }
                }
            }

            int thingy = 1;
            String eks = String.valueOf(32 - minx);
            String why = String.valueOf(32 - miny);

            if (symmetry.equals("8x32")) { eks = "32"; why = "8"; thingy = 2; minx = 16; miny = 16; }
            if (symmetry.equals("4x64")) { eks = "64"; why = "4"; thingy = 4; minx = 16; miny = 16; }
            if (symmetry.equals("2x128")) { eks = "128"; why = "2"; thingy = 8; minx = 16; miny = 16; }
            if (symmetry.equals("1x256")) { eks = "256"; why = "1"; thingy = 16; minx = 16; miny = 16; }

            if ((rulestring != null) && (rulestring.length() >= 1)) {
                if (topology != null) {
                    slashed += ":" + topology.replaceAll("p", "+").replaceAll("x", ",").replaceAll("f", "*");
                }
                writer.println("x = "+eks+", y = "+why+", rule = "+slashed);

                String sofar = "";

                for (int y = miny; y < 32; y++) {
                    for (int x = minx; x < 32; x++) {
                        if (soupArray[x][y] > 0) {
                            sofar = "o" + sofar;
                        } else {
                            sofar = "b" + sofar;
                        }
                    }
                    if (y == 31) {
                        writer.println(sofar + "!"); sofar = "";
                    } else if (((y + 1) % thingy) == 0) {
                        writer.println(sofar + "$"); sofar = "";
                    }
                }
            } else {
                for (int y = miny; y < 32; y++) {
                    for (int x = minx; x < 32; x++) {
                        if (soupArray[x][y] > 0) {
                            writer.print('*');
                        } else {
                            writer.print('.');
                        }
                    }
                    if (((y + 1) % thingy) == 0) {
                        writer.println();
                    }
                }
            }
        }
    }
}
