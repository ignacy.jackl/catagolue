package com.cp4space.catagolue.servlets;

import java.io.PrintWriter;
import java.lang.Iterable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map.Entry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.CommonNames;
import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.servlets.CensusServlet;
import com.cp4space.catagolue.utils.NumberUtils;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.storage.onestore.v3.OnestoreEntity.EntityProto;
import com.google.appengine.api.datastore.EntityTranslator;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;

public class HaulServlet extends ExampleServlet {
    
    @Override
    public String getTitle(HttpServletRequest req) { return CensusServlet.getRuleTitle(req, "Haul"); }

    public static int estimateEntitySize(Entity entity) {

        int objsize = 2048;
        Map<String, Object> props = entity.getProperties();

        for (Object obj : props.values()) {

            if (obj instanceof Blob) {
                objsize += ((Blob) obj).getBytes().length;
            } else if (obj instanceof Text) {
                objsize += ((Text) obj).getValue().length();
            } else {
                objsize += obj.toString().length();
            }
        }

        return objsize;

    }

    public static String insertSeparators(String orig, String sep, int erval) {

        if (orig.length() <= erval) {
            return orig;
        } else {
            return orig.substring(0, erval) + sep + insertSeparators(orig.substring(erval), sep, erval);
        }

    }

    @Override
    public void writeContent(PrintWriter writer, HttpServletRequest req) {

        String haulcode = req.getParameter("haulcode");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String committedString = req.getParameter("committed");
        String favmaxhauls = req.getParameter("maxhauls");
        String requestedName = req.getParameter("user");
        String pathinfo = req.getPathInfo();
        
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (haulcode == null)) {
                haulcode = pathParts[3];
            }
            if ((pathParts.length >= 5) && (committedString == null)) {
                committedString = pathParts[4];
            }
        }

        writeContentGeneric(writer, haulcode, rulestring, symmetry, committedString, favmaxhauls, requestedName, true);

    }

    public static void writeContentGeneric(PrintWriter writer, String haulcode, String rulestring, String symmetry, String committedString, String favmaxhauls, String requestedName, boolean useHtml) { 
        
        int committedInt = -1;

        if ((committedString != null) && (!committedString.equals(""))) {
            try {
                committedInt = Integer.valueOf(committedString.substring(0,1));
            } catch (NumberFormatException e) {
                // do nothing
            }
        }
        
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        
        if ((rulestring == null) || (rulestring.equals(""))) {

            if (useHtml) {
                writer.println("<p>You must specify a rulestring and symmetry type.</p>");
            } else {
                writer.println("You must specify a rulestring and symmetry type.");
            }
            
        } else if ((symmetry == null) || (symmetry.equals(""))) {

            if (useHtml) {
                CensusServlet.requestSymmetry(writer, rulestring, "/haul/");
            } else {
                writer.println("You must specify a rulestring and symmetry type.");
            }

        } else if ((haulcode == null) || (haulcode.equals(""))) {
            
            Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

            if (useHtml) {
                writer.println("<p>Here are the most recent hauls which have been uploaded by");
                writer.println("<a href=\"/apgsearch\">apgsearch</a>.</p>");
            }

            Query query;
            
            if (requestedName != null) {
                Filter userFilter = new FilterPredicate("displayedName", FilterOperator.EQUAL, requestedName);
                query = new Query("Haul", censusKey).setFilter(userFilter).addSort("date", Query.SortDirection.DESCENDING);
            } else if (committedInt >= 0) {
                Filter propertyFilter = new FilterPredicate("committed", FilterOperator.EQUAL, committedInt);
                query = new Query("Haul", censusKey).setFilter(propertyFilter).addSort("date", Query.SortDirection.DESCENDING);
            } else {
                query = new Query("Haul", censusKey).addSort("date", Query.SortDirection.DESCENDING);
            }

            int maxhauls = 100;

            if (!useHtml) {
                query.addProjection(new PropertyProjection("committed", Long.class));
                query.addProjection(new PropertyProjection("date", Date.class));
                if (requestedName == null) { query.addProjection(new PropertyProjection("displayedName", String.class)); }
            }

            if ((favmaxhauls != null) && ((!useHtml) || CensusServlet.isAdmin())) {
                maxhauls = Integer.valueOf(favmaxhauls);
            }

            PreparedQuery preparedQuery = datastore.prepare(query);
            Iterable<Entity> hauls = preparedQuery.asIterable(FetchOptions.Builder.withLimit(maxhauls));

            if (useHtml) {
                writer.println("<table cellspacing=1 border=2 cols=2>");
                writer.println("<tr><th>Root</th><th>Contributor<br><i>(verifier)</i></th><th>Soups</th><th>Objects</th><th>Date submitted</th><th>Version</th></tr>");
            }

            for (Entity haul : hauls) {
                
                String md5 = haul.getKey().getName();
                long precolour = (long) haul.getProperty("committed");

                String datestring = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format((Date) haul.getProperty("date"));

                String displayedName = requestedName;
                if (requestedName == null) { displayedName = (String) haul.getProperty("displayedName"); }
                if (displayedName == null) { displayedName = "null"; }

                if (!useHtml) {
                    writer.println("H" + String.valueOf(precolour) + " " + md5 + " " + datestring.replace(" ", "T") + " " + displayedName);
                    continue;
                }

                if (CensusServlet.isCensored(displayedName)) { displayedName = "Anonymous"; }

                displayedName = "<a href=\"/user/" + displayedName.replaceAll(" ", "%20") + "\">" + displayedName + "</a>";

                String root = (String) haul.getProperty("root");
                String verifierName = (String) haul.getProperty("verifierName");
                String version = (String) haul.getProperty("version");
                
                if (version == null) {
                    version = "(unknown)";
                }
                
                if (verifierName != null) {
                    verifierName = "<a href=\"/user/" + verifierName.replaceAll(" ", "%20") + "\">" + verifierName + "</a>";
                    displayedName = "<b>" + displayedName + "</b><br><i>(" + verifierName + ")</i>";
                }
                
                long numsoups = (long) haul.getProperty("numsoups");
                long numobjects = (long) haul.getProperty("numobjects");

                String linkcolour = "#7f0000";
                if (precolour == 0) {linkcolour = "#7f3f00"; }
                if (precolour == 1) {linkcolour = "#7f7f00"; }
                if (precolour == 2) {linkcolour = "#007f00"; }
                if (precolour == 3) {linkcolour = "#3f7f00"; }
                if (precolour == 6) {linkcolour = "#0000ff"; }

                root = insertSeparators(root, "<br>", 16);

                writer.println("<tr><td><a style=\"color:" + linkcolour + "\" href=\"/haul/"+rulestring+"/"+symmetry+"/"+md5+"\">");
                writer.println(root+"</a></td>");
                writer.println("<td>"+displayedName+"</td><td>"+String.valueOf(numsoups)+"</td><td>"+String.valueOf(numobjects)+"</td>");
                writer.println("<td>"+datestring+"</td>");
                writer.println("<td>"+version+"</td></tr>");
            }
            
            if (useHtml) { writer.println("</table>"); }
            
        } else {
            
            try {
                Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", haulcode).getKey();
                Entity haul = datastore.get(haulKey);

                Census census = new Census();
                
                String root = (String) haul.getProperty("root");

                String TheCensusData = Tabulation.getArbitraryData(haul, "censusData"); // ((Text) haul.getProperty("censusData")).getValue();
                String TheSoupData   = Tabulation.getArbitraryData(haul, "soupData"); // ((Text) haul.getProperty("soupData")).getValue();
                long numsoups = (long) haul.getProperty("numsoups");
                long numobjects = (long) haul.getProperty("numobjects");

                if (!useHtml) {

                    String md5 = haul.getKey().getName();
                    String version = (String) haul.getProperty("version");
                    if (version == null) { version = "(unknown)"; }

                    writer.println("@VERSION " + version);
                    writer.println("@MD5 " + md5);
                    writer.println("@ROOT " + root);
                    writer.println("@RULE " + rulestring);
                    writer.println("@SYMMETRY " + symmetry);
                    writer.println("@NUM_SOUPS " + String.valueOf(numsoups));
                    writer.println("@NUM_OBJECTS " + String.valueOf(numobjects));
                    writer.println("@CENSUS TABLE");
                    writer.println(TheCensusData);
                    writer.println("@SAMPLE_SOUPIDS");
                    writer.println(TheSoupData);

                    return;
                }

                if (CensusServlet.isAdmin()) {
                    if (committedString != null) {
                        haul.setProperty("committed", committedInt);
                        datastore.put(haul);
                        writer.println("<p><b>Haul commitment successfully changed to "+committedString+".</b></p>");
                    }

                    String errorMessage = (String) haul.getProperty("errorMessage");

                    if ((errorMessage != null) && (!(errorMessage.equals("success")))) {
                        writer.println("<p><b>Haul rejected for the following reason: "+errorMessage+".</b></p>");
                    }
                }
                
                census.affineAdd(TheCensusData, 1);
                census.includeSamples(TheSoupData, symmetry, root, null);
                
                String datestring = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z").format((Date) haul.getProperty("date"));
                String displayedName = (String) haul.getProperty("displayedName");
                String verifierName = (String) haul.getProperty("verifierName");
                
                int tablesize = census.table.size();
                
                if (displayedName == null) {
                    displayedName = "(null)";
                } else {
                    displayedName = "<a href=\"/user/" + displayedName.replaceAll(" ", "%20") + "\">" + displayedName + "</a>";
                }
                
                writer.println("<p>This haul was submitted by "+displayedName+" on "+datestring+".</p>");
                writer.println("<p>It contains " +
                                NumberUtils.commaChameleon(numobjects)+" objects (" +
                                NumberUtils.commaChameleon(tablesize) + " distinct) generated from " +
                                NumberUtils.commaChameleon(numsoups) + " soups.</p>");
                if (verifierName != null) {
                    verifierName = "<a href=\"/user/" + verifierName.replaceAll(" ", "%20") + "\">" + verifierName + "</a>";
                    writer.println("<p>The integrity of this haul was verified by "+verifierName+".</p>");
                }

                writer.println("<p>The seed root is " + root + "</p>");

                int hbytes = estimateEntitySize(haul);

                writer.println("<p>This haul occupies an estimated " + String.valueOf(hbytes >> 10) +
                        " kilobytes of memory, out of a maximum of 1000.</p>");
                
                boolean includeImages = (tablesize <= 500);
                
                writer.println("<table cellspacing=1 border=2 cols=2>");
                writer.println("<tr><th>Rank</th>"+(includeImages ? "<th>Image</th>" : "")+"<th>Object</th><th>Occurrences</th></tr>");
                
                Map<String, String> namemap = new HashMap<String, String>();
                CommonNames.getNamemap("std", namemap, true);
                
                int i = 0;
                
                for (Entry<String, Long> entry : census.sortedTable()) {
                    
                    i++;
                    
                    String apgcode = entry.getKey();
                    String demarcator = "<br />";
                    if (census.samples.containsKey(apgcode)) {
                        Set<String> sampleSoups = census.samples.get(apgcode);
                        for (String sampleSoup : sampleSoups) {
                            demarcator += " <a href=\"/hashsoup/" + sampleSoup + "/" + rulestring + "\">&bull;</a>";
                        }
                    } else {
                        demarcator = "";
                    }
                    if (namemap.containsKey(apgcode)) {
                        apgcode += " (" + namemap.get(apgcode) + ")";
                    }
                    SvgUtils.apgRow(includeImages, i, writer, rulestring, apgcode, NumberUtils.commaChameleon(entry.getValue()) + demarcator);
                }
                
                writer.println("</table>");
                
            } catch (EntityNotFoundException e) {
                writer.println("<p>The specified haul does not appear to exist. The most "
                        + "plausible explanation is that it was deleted after being committed "
                        + "to a census in order to save space.</p>");
            }
        }
    }
}
