package com.cp4space.catagolue.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Iterable;
import java.util.TreeSet;
import java.util.Map.Entry;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.census.Tabulation;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.utils.SvgUtils;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

public class TextCensusServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        resp.setContentType("text/plain");

        String tabprefix = req.getParameter("tabulation");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String pathinfo = req.getPathInfo();
        String sincedate = null;

        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (tabprefix == null)) {
                tabprefix = pathParts[3];
            }
            if ((pathParts.length >= 5) && (sincedate == null)) {
                sincedate = pathParts[4];
            }
        }

        PrintWriter writer = resp.getWriter();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

        if (rulestring == null) {
            writer.println("You must specify a rulestring.");
        } else if (symmetry == null) {
            writer.println("You must specify a symmetry type.");
        } else if ((tabprefix == null) || tabprefix.equals("sorted") || tabprefix.equals("objcount") || tabprefix.equals("since") || tabprefix.equals("summary")) {

            String countHeader = (symmetry.equals("synthesis-costs") ? "cost" : "occurrences");
            writer.println("\"apgcode\",\"" + countHeader + "\"");

            Key censusKey = KeyFactory.createKey("Census", rulestring + "/" + symmetry);

            Query query = new Query("Tabulation", censusKey);
            PreparedQuery preparedQuery = datastore.prepare(query);
            Iterable<Entity> tabulations = preparedQuery.asIterable(FetchOptions.Builder.withLimit(10000));

            if ((tabprefix != null) && tabprefix.equals("sorted")) {
                Census census = new Census();

                for (Entity tabulation : tabulations) {
                    census.affineAdd(Tabulation.getCensusData(tabulation), 1);
                }

                TreeSet<Entry<String,Long>> sortedTable = census.sortedTable();

                int i = 0;


                for (Entry<String, Long> entry : sortedTable) {
                    i++;
                    writer.println("\""+entry.getKey()+"\",\""+entry.getValue()+"\"");
                }
            } else {
                int totcount = 0;
                for (Entity tabulation : tabulations) {
                    String tabstring = Tabulation.getCensusData(tabulation);

                    // get object count:
                    int objcount = 1;
                    for (int j=0; j<tabstring.length(); j++) {
                        if (tabstring.charAt(j) == '\n') {
                            objcount++;
                        } 
                    }

                    if ((tabprefix == null) || ((objcount <= 100) && tabprefix.equals("summary"))) { 
                        tabstring = tabstring.replaceAll("\n", "\"\n\"").replaceAll(" ", "\",\"");
                        writer.print("\"");
                        writer.print(tabstring);
                        writer.print("\"\n");
                    } else if (tabprefix.equals("objcount") || tabprefix.equals("summary")) {
                        String tabname = tabulation.getKey().getName();
                        writer.println(String.valueOf(objcount)+" objects in tabulation "+tabname);
                    } else if (tabprefix.equals("since") && (sincedate != null)) {
                        Census census = new Census();
                        census.affineAdd(tabstring, 1);

                        for (int i = 0; i < 2; i++) {

                            if (i == 1) { sincedate = sincedate.substring(0, sincedate.length() - 2) + "01"; }

                            try {

                                String tabname = tabulation.getKey().getName();
                                Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry + "-" + sincedate).addChild("Tabulation", tabname).getKey();
                                Entity tabulation2 = datastore.get(tabulationKey);
                                String tabstring2 = Tabulation.getCensusData(tabulation2);
                                Census census2 = new Census();
                                census2.affineAdd(tabstring2, 1);
                                census.removeKeys(census2.table);
                                break;

                            } catch (EntityNotFoundException e) {
                            }

                        }

                        TreeSet<Entry<String,Long>> sortedTable = census.sortedTable();

                        for (Entry<String, Long> entry : sortedTable) {
                            writer.println("\""+entry.getKey()+"\",\""+entry.getValue()+"\"");
                        }

                    }
                    totcount += objcount;
                }
                if ((tabprefix != null) && (!tabprefix.equals("since"))) { writer.println("Total objects: "+String.valueOf(totcount)); }
            }
        } else {
            try {
                Key tabulationKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Tabulation", tabprefix).getKey();
                Entity tabulation = datastore.get(tabulationKey);
                BufferedReader bufReader = Tabulation.getCensusStream(tabulation);
                writer.println("\"apgcode\",\"occurrences\"");
                String line = null;
                while ((line = bufReader.readLine()) != null) {
                    writer.println("\"" + line.replace(" ", "\",\"") + "\"");
                }
            } catch (EntityNotFoundException e) {
                writer.println("Tabulation does not exist.");
            }
        }
    }
}
