package com.cp4space.catagolue.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cp4space.catagolue.servlets.CensusServlet;
import com.cp4space.catagolue.census.Census;
import com.cp4space.catagolue.census.Tabulation;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

public class RenameServlet extends HttpServlet {
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        
        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();

        if (!CensusServlet.isAdmin()) {
            writer.println("Renaming objects requires superuser privileges.");
            return;
        }

        String haulcode = req.getParameter("haulcode");
        String rulestring = req.getParameter("rule");
        String symmetry = req.getParameter("symmetry");
        String origname = req.getParameter("origname");
        String newname = req.getParameter("newname");
        String pathinfo = req.getPathInfo();
        
        if (pathinfo != null) {
            String[] pathParts = pathinfo.split("/");
            if ((pathParts.length >= 2) && (rulestring == null)) {
                rulestring = pathParts[1];
            }
            if ((pathParts.length >= 3) && (symmetry == null)) {
                symmetry = pathParts[2];
            }
            if ((pathParts.length >= 4) && (haulcode == null)) {
                haulcode = pathParts[3];
            }
            if ((pathParts.length >= 5) && (origname == null)) {
                origname = pathParts[4];
            }
            if ((pathParts.length >= 6) && (newname == null)) {
                newname = pathParts[5];
            }
        }
        
        if ((rulestring != null) && (symmetry != null) && (haulcode != null) && (origname != null) && (newname != null)) {
            
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            
            try {
                Key haulKey = new KeyFactory.Builder("Census", rulestring + "/" + symmetry).addChild("Haul", haulcode).getKey();
                Entity haul = datastore.get(haulKey);
                haul.setProperty("censusData", new Text(Tabulation.getArbitraryData(haul, "censusData").replaceAll(origname, newname)));
                haul.setProperty("soupData", new Text(Tabulation.getArbitraryData(haul, "soupData").replaceAll(origname, newname)));
                datastore.put(haul);
                writer.println("Haul find/replace complete.");
            } catch (EntityNotFoundException e) {
                writer.println("Haul not found.");
            }
        } else {
            writer.println("Insufficient information provided.");
        }
        
    }

}
